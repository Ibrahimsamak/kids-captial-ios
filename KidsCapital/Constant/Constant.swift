//
//  Constant.swift
//  Chalk Store
//
//  Created by iosMaher on 7/2/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation
import UIKit

struct Constant {
    
    static var HeaderAuth = ["Accept" : "application/json",
                             "Accept-Language" : Auth_User._Language]
    
    static var HeaderToken = ["Accept" : "application/json",
                              "Accept-Language" : Auth_User._Language,
                              "Authorization" : "Bearer \(Auth_User._Token)"]

    static var HeaderTokenLang = ["Accept" : "application/json",
                                  "Accept-Language" : Auth_User._Language,
                                  "Authorization" : "Bearer \(Auth_User._Token)"]

    static var HeaderUpdate = ["Accept" : "application/json",
                               "Content-Type" : "application/x-www-form-urlencoded",
                               "Accept-Language" : Auth_User._Language,
                               "Authorization" : "Bearer \(Auth_User._Token)"]

    // Site Domain + Api
    static let SiteDomain                    = "http://kids-capital.com/api/"
    static let ApiDomain                     =  SiteDomain + "user/"
    
    // Auth
    static let LoginURL                      =  ApiDomain + "login"
    static let ForgetURL                     =  ApiDomain + "forget"
    static let RegisterURL                   =  ApiDomain + "register"
    static let Logout                        =  ApiDomain + "logout"
    static let ResetURL                      =  ApiDomain + "reset"
    static let ChangePassword                =  ApiDomain + "change-password"

    // Topic Key
    
    //Get Requests
    static let SponsorsURL                   =  ApiDomain + "sponsors"
    static let NurseriesURL                  =  ApiDomain + "nurseries"
    
    static let AreaURL                       =  ApiDomain + "areas?"
    static let ProvincesURL                  =  ApiDomain + "provinces?"
    static let CategoryURL                   =  ApiDomain + "categories?"
    static let FacilitiesURL                 =  ApiDomain + "facilities?"
    
    static let GalleriesURL                  = ApiDomain + "nurseries/galleries/"
    static let AdsCategoriesURL              = ApiDomain + "ads-categories?"
    static let AdsListURL                    = ApiDomain + "ads"
    static let MyAdsListURL                  = ApiDomain + "ads/my"

    static let NewsURL                       = ApiDomain + "news"
    static let PagesURL                      = ApiDomain + "pages"
    static let ContactsURL                   = ApiDomain + "contacts"
    static let CouponsURL                    = ApiDomain + "coupons"
    static let MyCouponsURL                  = ApiDomain + "coupons/my"

    static let ClaimCouponsURL               = ApiDomain + "coupons/claim/"
    static let MemberClaimURL                = ApiDomain + "coupon/use"

    static let EidtProfileURL                = ApiDomain + "profile"

    // Number of view
    static let NovAds                        = ApiDomain + "ads/nov/"
    static let NovNurseries                  = ApiDomain + "nurseries/nov/"
    static let NovNews                       = ApiDomain + "news/nov/"
    static let NovSponsor                    = ApiDomain + "sponsors/nov/"

    
    // Update Device Token
    static let DeviceToken                   = ApiDomain + "update-token"
    
    static let ShareURL                      = "http://onelink.to/9fghvw"

}
