//
//  MEAdsCategoriesCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/6/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEAdsCategoriesCell: UICollectionViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Ads: UIImageView!
    @IBOutlet weak var view_Shadow: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addShadowView(width: 0,
                           height: 12,
                           opacity: 0.4,
                           radius: 14,
                           color: "000000".color.withAlphaComponent(0.4),cornerRadius: 0)
        view_Shadow.setRounded(radius: 4)
    }

}
