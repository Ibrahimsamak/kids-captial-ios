//
//  MEFacilityCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/9/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEFacilityCell: UICollectionViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_FacilityName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
