//
//  MECouponsCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MECouponsCell: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Coupon: UIImageView!
    @IBOutlet weak var lbl_Date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_Title.switchDirection()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
