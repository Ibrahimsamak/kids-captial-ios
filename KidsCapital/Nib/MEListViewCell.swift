//
//  MEListViewCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/3/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEListViewCell: UITableViewCell {

    @IBOutlet weak var view_OpenReg: UIView!
    
    @IBOutlet weak var view_Special: UIView!
    @IBOutlet weak var view_Shadow: UIView!
    
    @IBOutlet weak var lbl_Distance: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!

    @IBOutlet weak var lbl_Special: UILabel!

    @IBOutlet weak var img_SpecialFlag: UIImageView!
    @IBOutlet weak var img_Logo: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var _FacilityArray : [FacilityST] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_Title.switchDirection()
        lbl_Content.switchDirection()
        
        lbl_Special.text = Auth_User._Language == "ar" ? "مميز" : "S  P  E  C  I  A  L"
        
        view_Shadow.setRounded(radius: 4)
        view_Shadow.clipsToBounds = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MEFacilityCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension MEListViewCell : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _FacilityArray.count
    }
}

extension MEListViewCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MEFacilityCell = collectionView.dequeueCVCell(indexPath: indexPath)
        let index = _FacilityArray[indexPath.row]
        
        cell.lbl_FacilityName.text = index._name
        cell.img_Icon.fetchImage(index._icon)
        
        return cell
    }
}

extension MEListViewCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 30)
    }
}
