//
//  MEMyAdsCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEMyAdsCell: UITableViewCell {

    @IBOutlet weak var view_Shadow: UIView!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var img_Ads: UIImageView!
    @IBOutlet weak var img_Status: UIImageView!
    @IBOutlet weak var lbl_Status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_Content.switchDirection()
        lbl_Content.switchDirection()
        
        img_Ads.roundCorners(corners: [.topLeft,.bottomLeft], radius: 4)
        view_Shadow.addShadowView(width: 0, height: 5, opacity: 0.1, radius: 8, color: UIColor.black,cornerRadius: 4)
//        view_Shadow.setRounded(radius: 4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
