//
//  MESearchTypeCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/3/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MESearchTypeCell: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Checked: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_Checked.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
