//
//  MENewsCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MENewsCell: UITableViewCell {

    @IBOutlet weak var view_Shadow: UIView!
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_ShortDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lbl_Title.switchDirection()
        lbl_ShortDesc.switchDirection()
        
        view_Shadow.addShadowView(width: 0, height: 5, opacity: 0.1, radius: 8, color: UIColor.black,cornerRadius: 4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
