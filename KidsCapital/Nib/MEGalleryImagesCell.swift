//
//  MEGalleryImagesCell.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEGalleryImagesCell: UICollectionViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        img_Image.setRounded(radius: 8)
    }

}
