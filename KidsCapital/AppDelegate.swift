//
//  AppDelegate.swift
//  KidsCapital
//
//  Created by iosMaher on 11/29/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    static let MainSB = UIStoryboard(name: "Main", bundle: nil)

    override init() {
        FirebaseApp.configure()
        
        if Auth_User._firstOpen {
            Language.setAppLanguage(lang: "ar")
            Auth_User._Language = "ar"
            Localizer.DoTheExchange()
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            Auth_User._firstOpen = false
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey("AIzaSyAUtkc3WIz_i4lnw1o49p1ZQ3CBf2QA6UE")
        GMSPlacesClient.provideAPIKey("AIzaSyAUtkc3WIz_i4lnw1o49p1ZQ3CBf2QA6UE")
        
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
        
        DidOpenView._AdsCategoriesOpened = false
        DidOpenView._FacilitesOpened = false
        DidOpenView._CategoryOpened = false
        DidOpenView._ProvinceOpened = false
        DidOpenView._AreaOpened = false
        
        printAllFont()
//        iniGoogleAnaltics()
        getSponsor()
        Appearance()

        return true
    }
    
    //MARK: google analtyics in appdelegate
    func iniGoogleAnaltics(){
        if  let gai = GAI.sharedInstance(),
        let gaConfigValues = Bundle.main.infoDictionary?["GoogleAnalytics"] as? [String: String],
        let trackingId = gaConfigValues["TRACKING_ID"]
    {
        //gai.logger.logLevel = .verbose
        gai.trackUncaughtExceptions = true
        gai.tracker(withTrackingId: trackingId)
        //gai.dispatchInterval = 120.0
        }
    }
    
    func getSponsor() {
        
        let url = Constant.SponsorsURL
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                return
            }else{

                guard let res = response.dictionary else { return }
                let _imageArray = res["data"]?.array ?? []

                var tempArray : [[String : Any]] = []
                for img in _imageArray {
                    let cusObj = img.dictionaryObject
                    tempArray.append(cusObj ?? [:])
                }
                
                Cached_Data._SponsoredArray = tempArray
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }

    func Appearance() {
        
        IQKeyboardManager.shared.enable = true
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        let backArrowImage = UIImage(named: "icon_BackArrow")
        let renderedImage = backArrowImage?.withRenderingMode(.alwaysOriginal)
        
        UINavigationBar.appearance().barTintColor = "000000".color
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "CircularStd-Black", size: 14)!,NSAttributedString.Key.foregroundColor:UIColor.white]
        
        UINavigationBar.appearance().backIndicatorImage = renderedImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = renderedImage
    }

    func printAllFont() {
        for familyName in UIFont.familyNames {
            for fontName in UIFont.fontNames(forFamilyName: familyName ) {
                print("\(familyName) : \(fontName)")
            }
        }
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        let userInfo = notification.request.content.userInfo
        let state = UIApplication.shared.applicationState
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print(deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let aps = userInfo["aps"] as? [String:Any] {
            if let alert = aps["alert"] as? [String:Any] {
                
            }
        }
        print(userInfo)
    }
}

extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        InstanceID.instanceID().instanceID { (token, error) in
            if error == nil {
                if Auth_User._Token != "" {
                    self.updateFCMToken((token?.token ?? ""))
                }
            }
        }
        connectToFcm()
    }
    
    func updateFCMToken(_ token : String) {
        
        let url = Constant.DeviceToken
        let auth = Constant.HeaderUpdate
        let param = ["device_token" : token] as [String : Any]
        
        MyApi.api.putRequestWihtoutValidate(url: url, para: param, auth: auth) { (error, response) in
            print(response)
        }
    }
    
    func connectToFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print(fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
}
