//
//  MESponsoredVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/2/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher
import SDWebImage
import SwiftyJSON
import Firebase

class MESponsoredVC: UIViewController {


    @IBOutlet weak var img_Sponsore: UIImageView!

//    var _SponsoredArray : [SponsoredST] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        Analytics.setScreenName("Sponsor Page", screenClass: "Sponsor + Sponsor title")
        
        appendSponsored()
    }
    
    func appendSponsored() {
        
        let obj = SponsoredST(_Obj: Cached_Data._SponsoredArray.first ?? [:])
        
        let url = URL(string: obj._image)
        self.img_Sponsore.sd_setIndicatorStyle(.gray)
        self.img_Sponsore.sd_setShowActivityIndicatorView(true)
        self.img_Sponsore.sd_setImage(with: url, placeholderImage: UIImage(), options: .refreshCached, completed: nil)
        
        getNov(id: obj._id)
    }
    
    func getNov(id: Int) {
        
        let url = Constant.NovSponsor + "\(id)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                guard let res = response.dictionary else { return }
                let nov = res["nov"]?.int ?? 0
                print(nov)
            }
        }
    }

    
    @IBAction func GoHomeAction(_ sender : UIButton) {
        let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
        self.RootViewController(viewController: vc)
    }
    
}
