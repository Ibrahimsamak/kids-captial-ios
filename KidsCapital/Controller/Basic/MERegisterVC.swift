//
//  MERegisterVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/15/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import SafariServices
import Firebase

class MERegisterVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txt_Fullname: UITextField!
    @IBOutlet weak var txt_Username: UITextField!
    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    @IBOutlet weak var txt_RePass: UITextField!
    
    @IBOutlet var collection_text: [UITextField]!
    @IBOutlet var collection_label: [UILabel]!

    var _selected = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "مستخدم جديد".localized
        txt_Username.delegate = self

        switchDirection()
    }
    
    func switchDirection() {
        for txt in collection_text {
            txt.switchDirection()
        }
        
        for lbl in collection_label {
            lbl.switchDirection()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.containsWhitespace {
            return false
        }else if string.containtSpecialCharacter {
            return false
        }
        
        let old_text = textField.text ?? ""
        if string == "." {
            var array = Array(old_text)
            array = array.filter { $0 == "." }
            return array.count == 1 ?  false : true
        }
        if string == "_" {
            var array = Array(old_text)
            array = array.filter { $0 == "_" }
            return array.count == 1 ?  false : true
        }
        return true
    }

    @IBAction func RegisterAction(_ sender : UIButton) {
        
        guard let Fullname = txt_Fullname.text?.TrimWhiteSpaces, !Fullname.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.FullnameRequired())
             return
        }
        
        guard let Username = txt_Username.text?.TrimWhiteSpaces, !Username.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.UsernameRequired())
            return
        }

        guard let Email = txt_Email.text?.TrimWhiteSpaces, !Email.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.EmailRequired())
            return
        }

        guard Email.isEmailValid else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.InvalidEmail())
            return
        }

        guard let Phone = txt_Phone.text?.TrimWhiteSpaces, !Phone.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PhoneRequired())
            return
        }
        
        guard let Password = txt_Password.text, !Password.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PasswordRequired())
            return
        }
        
        guard let RePass = txt_RePass.text, !RePass.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ConfirmPasswordRequired())
            return
        }
     
        guard Password == RePass else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PasswordsNotMatch())
            return
        }

        guard _selected else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.AcceptTerms())
            return
        }
        
        InstanceID.instanceID().instanceID { (token, error) in
            
            let url = Constant.RegisterURL
            let auth = Constant.HeaderAuth
            let param = ["email" : Email,
                         "username" : Username,
                         "mobile" : Phone,
                         "name" : Fullname,
                         "password" : Password,
                         "password_confirmation" : RePass,
                         "device_token" : (token?.token ?? "")]
            
            self.showIndicator()
            self.view.endEditing(true)
            MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
                if error != nil {
                    self.hideIndicator()
                    self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                    return
                }else{
                    
                    self.hideIndicator()
                    guard let res = response.dictionary else { return }
                    let _data = res["data"]?.dictionaryObject ?? [:]
                    let errors = res["errors"]?.dictionaryObject ?? [:]
                    
                    if errors.isEmpty {
                        let access_token = res["access_token"]?.string ?? ""
                        Auth_User._Token = access_token
                        Auth_User.ـUserInfo = _data
                        Constant.HeaderTokenLang.updateValue("Bearer \(access_token)", forKey: "Authorization")
                        Constant.HeaderToken.updateValue("Bearer \(access_token)", forKey: "Authorization")
                        
                        let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
                        self.RootViewController(viewController: vc)
                    }else{
                        self.showOkAlert(title: "", message: errors.printErrors())
                    }
                }
            }
        }
    }
    
    @IBAction func TermsAction(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            _selected = true
            sender.setImage("icon_AcceptTerms".toImage, for: .normal)
        }else{
            _selected = false
            sender.setImage("icon_ChecboxNS".toImage, for: .normal)
        }
    }

    @IBAction func OpenTerms(_ sender : UIButton) {
        if let url = URL(string: "http://kids-capital.com/terms") {
            let svc = SFSafariViewController(url: url)
            self.present(svc, animated: true, completion: nil)
        }
    }
}
