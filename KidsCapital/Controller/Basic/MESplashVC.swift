//
//  MESplashVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/30/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

var _newCoupon = false

class MESplashVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        getCoupons()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if Cached_Data._SponsoredArray.isEmpty {
                let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
                self.RootViewController(viewController: vc)
            }else{
                let vc : MESponsoredVC = AppDelegate.MainSB.instanceVC()
                self.RootViewController(viewController: vc)                
            }
        }
    }
    
    func getCoupons() {
        
        let url = Constant.CouponsURL
        let auth = Constant.HeaderTokenLang
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            guard let res = response.dictionary else { return }
            let _dataArray = res["data"]?.array ?? []
            _newCoupon = _dataArray.isEmpty ? false : true
        }
    }

    
}
