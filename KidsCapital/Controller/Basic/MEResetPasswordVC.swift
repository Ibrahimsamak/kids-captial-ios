//
//  MEResetPasswordVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/28/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEResetPasswordVC: UIViewController {

    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var txt_Code: UITextField!
    @IBOutlet weak var txt_NewPass: UITextField!
    @IBOutlet weak var txt_ConfirmPass: UITextField!
    
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var lbl_NewPass: UILabel!
    @IBOutlet weak var lbl_ConfirmPass: UILabel!

    var _email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "RESET PASSWORD".localized
        txt_Email.text = _email
        
        txt_Email.switchDirection()
        txt_Code.switchDirection()
        txt_NewPass.switchDirection()
        txt_ConfirmPass.switchDirection()
        
        lbl_Email.switchDirection()
        lbl_Code.switchDirection()
        lbl_NewPass.switchDirection()
        lbl_ConfirmPass.switchDirection()

    }
    
    
    @IBAction func ResetAction(_ sender: UIButton) {
        
        guard let Code = txt_Code.text?.TrimWhiteSpaces, !Code.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ResetCode())
            return
        }
        
        guard let Password = txt_NewPass.text, !Password.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.NewPasswordRequired())
            return
        }
        
        guard let RePass = txt_ConfirmPass.text, !RePass.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ConfirmPasswordRequired())
            return
        }
        
        guard Password == RePass else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PasswordsNotMatch())
            return
        }

        self.view.endEditing(true)
        self.showIndicator()
        
        let url = Constant.ResetURL
        let auth = Constant.HeaderAuth
        let param = ["email" :_email,
                     "code" : Code,
                     "password" : Password,
                     "password_confirmation" : Password]
        
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let message = res["message"]?.string ?? ""
                let errors = res["errors"]?.dictionaryObject ?? [:]

                if errors.isEmpty {
                    self.showOkAlertWithComp(title: "", message: message, completion: { (_) in
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
    }
    
    
}
