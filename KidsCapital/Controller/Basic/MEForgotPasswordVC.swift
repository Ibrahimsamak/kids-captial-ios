//
//  MEForgotPasswordVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/15/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEForgotPasswordVC: UIViewController {

    @IBOutlet weak var txt_Email: UITextField!
    @IBOutlet weak var lbl_Email: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        title = "FORGOT PASSWORD".localized
        
        txt_Email.switchDirection()
        lbl_Email.switchDirection()
        
        self.navigationItem.hideBackWord()

    }
    
    @IBAction func ResetAction(_ sender : UIButton) {
        
        guard let Email = txt_Email.text?.TrimWhiteSpaces, !Email.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.EmailRequired())
            return
        }
        
        guard Email.isEmailValid else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.InvalidEmail())
            return
        }
        
        let url = Constant.ForgetURL
        let auth = Constant.HeaderAuth
        let param = ["email" : Email]

        self.showIndicator()
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let errors = res["errors"]?.dictionaryObject ?? [:]

                if errors.isEmpty {
                    let vc : MEResetPasswordVC = AppDelegate.MainSB.instanceVC()
                    vc._email = Email
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
    }
    
}
