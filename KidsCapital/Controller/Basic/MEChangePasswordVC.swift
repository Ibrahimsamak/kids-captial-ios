//
//  MEChangePasswordVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/15/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEChangePasswordVC: UIViewController {

    @IBOutlet weak var txt_OldPass: UITextField!
    @IBOutlet weak var txt_NewPass: UITextField!
    @IBOutlet weak var txt_RePass: UITextField!

    @IBOutlet weak var lbl_OldPass: UILabel!
    @IBOutlet weak var lbl_NewPass: UILabel!
    @IBOutlet weak var lbl_RePass: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "RESET PASSWORD".localized
        
        txt_OldPass.switchDirection()
        txt_NewPass.switchDirection()
        txt_RePass.switchDirection()
        
        lbl_OldPass.switchDirection()
        lbl_NewPass.switchDirection()
        lbl_RePass.switchDirection()
    }
    
    @IBAction func ChangePassAction(_ sender : UIButton) {
     
        guard let OldPass = txt_OldPass.text, !OldPass.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.OldPasswordRequired())
            return
        }

        guard let Password = txt_NewPass.text, !Password.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.NewPasswordRequired())
            return
        }
        
        guard let RePass = txt_RePass.text, !RePass.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ConfirmPasswordRequired())
            return
        }
        
        guard Password == RePass else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PasswordsNotMatch())
            return
        }

        let url = Constant.ChangePassword
        let auth = Constant.HeaderTokenLang
        let param = ["old_password" : OldPass,
                     "password" : Password,
                     "password_confirmation" : RePass]
        
        self.view.endEditing(true)
        self.showIndicator()
        MyApi.api.putRequestWihtoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let errors = res["errors"]?.dictionaryObject ?? [:]
                let message = res["message"]?.string ?? ""
                
                if errors.isEmpty {
                    self.showOkAlertWithComp(title: "", message: message, completion: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
        
    }
    
    
}
