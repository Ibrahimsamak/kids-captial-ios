//
//  MELoginVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import SafariServices
import Firebase

class MELoginVC: UIViewController {

    @IBOutlet weak var txt_UsernameEmail: UITextField!
    @IBOutlet weak var txt_Password: UITextField!
    
    @IBOutlet weak var lbl_Username: UILabel!
    @IBOutlet weak var lbl_Password: UILabel!

    @IBOutlet weak var btn_ForgotPass: UIButton!
    @IBOutlet weak var btn_Register: UIButton!
    
    @IBOutlet weak var btn_TermsAgree: UIButton!
    
    var _selected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "SIGN IN".localized
        self.navigationItem.hideBackWord()
        
        txt_UsernameEmail.switchDirection()
        txt_Password.switchDirection()
        lbl_Username.switchDirection()
        lbl_Password.switchDirection()
        
        btn_Attrib()
    }
    
    func btn_Attrib() {
        
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                  NSAttributedString.Key.font : UIFont.init(name: "CircularStd-Black", size: 16)!,
                                  NSAttributedString.Key.foregroundColor : "FFDD00".color] as [NSAttributedString.Key : Any]

        let forgotAttributedString = NSAttributedString(string: "Forgot Password?".localized, attributes: underlineAttribute)
        let registerAttributedString = NSAttributedString(string: "Don’t Have Account?".localized, attributes: underlineAttribute)

        btn_ForgotPass.setAttributedTitle(forgotAttributedString, for: .normal)
        btn_Register.setAttributedTitle(registerAttributedString, for: .normal)

    }
    
    @IBAction func LoginAction(_ sender : UIButton) {
        
        guard let UserEmail = txt_UsernameEmail.text?.TrimWhiteSpaces, !UserEmail.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.UserEmailRequired())
            return
        }
        
        guard let Password = txt_Password.text, !Password.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PasswordRequired())
            return
        }
        
        guard _selected else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.AcceptTerms())
            return
        }
        
        InstanceID.instanceID().instanceID { (token, error) in
            
            let url = Constant.LoginURL
            let auth = Constant.HeaderAuth
            let param = ["email_or_username" : UserEmail,
                         "password" : Password,
                         "device_token" : (token?.token ?? "")]
            
            self.view.endEditing(true)
            self.showIndicator()
            MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
                if error != nil {
                    self.hideIndicator()
                    self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                    return
                }else{
                    
                    self.hideIndicator()
                    guard let res = response.dictionary else { return }
                    let _data = res["data"]?.dictionaryObject ?? [:]
                    let errors = res["errors"]?.dictionaryObject ?? [:]
                    
                    print(res)
                    if errors.isEmpty {
                        let access_token = res["access_token"]?.string ?? ""
                        let user_type = _data["user_type"] as? Int ?? -1
                        
                        Auth_User._UserType = user_type
                        Auth_User._Token = access_token
                        Auth_User.ـUserInfo = _data
                        
                        Constant.HeaderTokenLang.updateValue("Bearer \(access_token)", forKey: "Authorization")
                        Constant.HeaderToken.updateValue("Bearer \(access_token)", forKey: "Authorization")

                        let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
                        self.RootViewController(viewController: vc)
                    }else{
                        self.showOkAlert(title: "", message: errors.printErrors())
                    }
                }
            }
        }
    }
    
    @IBAction func ForgotPassAction(_ sender : UIButton) {
        let vc : MEForgotPasswordVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func RegisterAction(_ sender : UIButton) {
        let vc : MERegisterVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func TermsAction(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            _selected = true
            sender.setImage("icon_AcceptTerms".toImage, for: .normal)
        }else{
            _selected = false
            sender.setImage("icon_ChecboxNS".toImage, for: .normal)
        }
    }
    
    @IBAction func OpenTerms(_ sender : UIButton) {
        if let url = URL(string: "http://kids-capital.com/terms") {
            let svc = SFSafariViewController(url: url)
            self.present(svc, animated: true, completion: nil)
        }
    }

}
