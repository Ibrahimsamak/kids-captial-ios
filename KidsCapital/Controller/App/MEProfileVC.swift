//
//  MEProfileVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/14/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEProfileVC: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIButtonScrollView!
    
    @IBOutlet weak var img_CouponOff: UIImageView!
    @IBOutlet weak var img_CouponOn: UIImageView!
    
    @IBOutlet weak var lbl_Fullname: UILabel!
    @IBOutlet weak var lbl_NameTitle: UILabel!
    @IBOutlet weak var lbl_Username: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_ChangePass: UILabel!
    
    @IBOutlet weak var stack_NotLogged: UIStackView!
    @IBOutlet weak var view_Logged: UIView!
    
    @IBOutlet weak var btn_ContactUsOff: UIButton!
    @IBOutlet weak var btn_NewAdsOff: UIButton!
    @IBOutlet weak var btn_CouponsOff: UIButton!
    
    @IBOutlet weak var btn_Fullname: UIButton!
    @IBOutlet weak var btn_Username: UIButton!
    @IBOutlet weak var btn_PhoneNumber: UIButton!
    @IBOutlet weak var btn_MyAds: UIButton!
    @IBOutlet weak var btn_NewAds: UIButton!
    @IBOutlet weak var btn_ContactUs: UIButton!
    @IBOutlet weak var btn_ChangePassword: UIButton!
    @IBOutlet weak var btn_Logout: UIButton!
    @IBOutlet weak var btn_Coupons: UIButton!
    @IBOutlet weak var btn_MyCoupons: UIButton!
    
    @IBOutlet weak var view_MyAds: UIView!
    @IBOutlet weak var view_Coupon: UIView!
    
    var _userInfo : UserST?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth_User._Token != "" {
            self._userInfo = UserST(Auth_User.ـUserInfo)
            setUserInfo()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "MY ACCOUNT".localized
        self.navigationItem.hideBackWord()
        scrollView.delegate = self

        if Auth_User._Token != "" {
            stack_NotLogged.isHidden = true
            view_Logged.isHidden = false
        }else{
            stack_NotLogged.isHidden = false
            view_Logged.isHidden = true
            btn_Logout.isHidden = true
        }
        
        switchDirection()
        getCoupons()
    }
    
    func switchDirection() {
        lbl_Fullname.switchDirection()
        lbl_Username.switchDirection()
        lbl_Email.switchDirection()
        lbl_Phone.switchDirection()
        lbl_ChangePass.switchDirection()
        
    }
    
    func setUserInfo() {
        
        let _nameAr = "Hi".localized + " \(_userInfo?._name ?? "")"
        let _nameEn = "\(_userInfo?._name ?? "") " + "Hi".localized
        
        let name = Auth_User._Language == "ar" ? _nameAr :_nameEn
        self.lbl_NameTitle.text = name
        
        self.lbl_Fullname.text = _userInfo?._name
        self.lbl_Username.text = _userInfo?._username
        self.lbl_Email.text = _userInfo?._email
        self.lbl_Phone.text = _userInfo?._mobile
        
        if _userInfo?._user_type == 1 {
            self.view_Coupon.isHidden = true
        }
    }
    
    @IBAction func SignInAction(_ sender : UIButton) {
        let vc : MELoginVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func LogoutAction(_ sender : UIButton) {
        
        self.showCustomAlert(okFlag: false, title: "", message: Auth_Strings.shared.Logout(), okTitle: "Yes".localized, cancelTitle: "No".localized) { (action) in
            if action {
                self.Logout()
            }
        }
        
    }
    
    @IBAction func MultiAction(_ sender : UIButton) {
        switch sender {
        case btn_ContactUsOff, btn_ContactUs:
            let vc : MEContactUsVC = AppDelegate.MainSB.instanceVC()
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_CouponsOff, btn_NewAdsOff:
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            
        case btn_MyAds:
            let vc : MEMyAdsVC = AppDelegate.MainSB.instanceVC()
            vc._isMyAds = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_Fullname:
            let vc : MEEditProfileVC = AppDelegate.MainSB.instanceVC()
            vc._type = .Name
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_PhoneNumber:
            let vc : MEEditProfileVC = AppDelegate.MainSB.instanceVC()
            vc._type = .Phone
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_Username:
            let vc : MEEditProfileVC = AppDelegate.MainSB.instanceVC()
            vc._type = .Username
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_ChangePassword:
            let vc : MEChangePasswordVC = AppDelegate.MainSB.instanceVC()
            self.navigationController?.pushViewController(vc, animated: true)
            
        case btn_NewAds:
            let vc : MENewAdVC = AppDelegate.MainSB.instanceVC()
            self.navigationController?.pushViewController(vc, animated: true)

        case btn_Coupons:
            if _userInfo?._user_type != 1 {
                let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc : MEClaimCouponVC = AppDelegate.MainSB.instanceVC()
                self.navigationController?.pushViewController(vc, animated: true)
            }

        case btn_MyCoupons:
            let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
            vc._myCoupon = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            break
        }
    }
    
    func getCoupons() {
        
        let url = Constant.CouponsURL
        let auth = Constant.HeaderTokenLang
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            guard let res = response.dictionary else { return }
            let _dataArray = res["data"]?.array ?? []
            _newCoupon = _dataArray.isEmpty ? false : true
            self.isNewCoupon()
        }
    }
    
    func isNewCoupon() {
        let img = Auth_User._Token == "" ? img_CouponOff : img_CouponOn
        let _newCou = _newCoupon ? "icon_Coupon".toImage : "icon_NoCoupons".toImage
        if _newCoupon {
            img?.image = _newCou
        }else{
            img?.image = _newCou
        }
    }
    
    func Logout() {
        
        let url = Constant.Logout
        let auth = Constant.HeaderToken
        
        self.showIndicator()
        MyApi.api.postReqWithHeader(url: url, para: [:], auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let errors = res["errors"]?.dictionaryObject ?? [:]
                
                if errors.isEmpty {
                    Auth_User.removeUserInfo()
                    let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
                    self.RootViewController(viewController: vc)
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }

            }
        }
        
    }
    
}

class UIButtonScrollView: UIScrollView {
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIButton {
            return true
        }
        return super.touchesShouldCancel(in: view)
    }

}
