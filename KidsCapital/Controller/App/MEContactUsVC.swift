//
//  MEContactUsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/14/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import MessageUI

class MEContactUsVC: UIViewController , MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout_TVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btn_Site: UIButton!
    @IBOutlet weak var btn_Mail: UIButton!
    @IBOutlet weak var btn_Call: UIButton!
    @IBOutlet weak var btn_Share: UIButton!

    @IBOutlet weak var view_Coupon: UIView!

    var _ContactUs : ContactST?
    var _pageArray : [PageST] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "CONTACT US".localized
        self.navigationItem.hideBackWord()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCell(id: "MEContactUsCell")
        
        
        if !DidOpenView._PagesOpened {
            DidOpenView._PagesOpened = true
            
            if Cached_Data._PagesArray.isEmpty {
                self.showIndicator()
            }else{
                appendPages()
            }
            
            getPages()
        }else{
            appendPages()
        }
        
        self._ContactUs = ContactST(_Obj: Cached_Data._ContactUsObject)
        getContactUs()
        
    }
    
    func getContactUs() {
    
        let url = Constant.ContactsURL
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionaryObject ?? [:]
                
                Cached_Data._ContactUsObject = _data
                self._ContactUs = ContactST(_Obj: Cached_Data._ContactUsObject)
            }
        }
    }
    
    func getPages() {
        
        let timeStamp = Date().toMillis()!
        let url = Constant.PagesURL + "?date=\(timeStamp)"
        let auth = Constant.HeaderAuth

        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject ?? [:]}
                var _oldItems = Cached_Data._PagesArray
                
                guard !ud_tempArray.isEmpty else {
                    return
                }

                if _oldItems.isEmpty {
                    Cached_Data._PagesArray = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._PagesArray = _oldItems
                }

                self.appendPages()
            }
        }
    }
    
    func appendPages() {
        _pageArray = Cached_Data._PagesArray.map({ (item) -> PageST in
            return PageST(_Obj: item)
        })
        self.layout_TVHeight.constant = CGFloat(self._pageArray.count * 50)
        self.tableView.reloadData()
    }
    
    
    @IBAction func MultiAction(_ sender : UIButton) {
        
        switch sender {
        case btn_Site:
            
            guard !(_ContactUs?._website.isEmpty)! else {
                self.showOkAlert(title: "", message: Auth_Strings.shared.NoWebsite())
                return
            }
            
            OpenSocialMedia(link: _ContactUs?._website ?? "")
            
        case btn_Mail:
            sendEmail(email: _ContactUs?._email ?? "")
            
        case btn_Call:
            let link = "tel://\(self._ContactUs?._phone ?? "")"
            OpenSocialMedia(link: link)
            
        default:
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [Constant.ShareURL], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = (sender)
            activityViewController.popoverPresentationController?.permittedArrowDirections = .any
            self.present(activityViewController, animated: true)

        }
    }
    
    func OpenSocialMedia(link: String) {
        if let _url = URL(string: link) {
            if UIApplication.shared.canOpenURL(_url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(_url, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(_url)
                }
            }
            else {
                print("error")
            }
        }
    }
    
    func sendEmail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setSubject("Send Mail")
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension MEContactUsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _pageArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _pageArray[indexPath.row]
        let vc : METermsConditionsVC = AppDelegate.MainSB.instanceVC()
        vc.Obj = index
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MEContactUsVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MEContactUsCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        cell.lbl_Title.text = _pageArray[indexPath.row]._title
        return cell
    }
}
