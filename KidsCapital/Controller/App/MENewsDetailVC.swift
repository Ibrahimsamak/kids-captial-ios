//
//  MENewsDetailVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class MENewsDetailVC: UIViewController {

    @IBOutlet weak var img_ImageView: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    
    @IBOutlet weak var lbl_ViewCount: UILabel!
    @IBOutlet weak var view_Separator: UIView!
    @IBOutlet weak var img_Nov: UIImageView!

    @IBOutlet weak var view_Coupon: UIView!

    var _Obj : NewsST!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "NEWS DETAILS".localized
        self.navigationItem.hideBackWord()
        
//        Analytics.setScreenName("News details page", screenClass: "News details page")

        lbl_Title.switchDirection()
        lbl_Desc.switchDirection()
        
        setInfo()
        getFullDesc()
        getNov()
    }
    
    func setInfo() {
        lbl_Title.text = _Obj._title
        lbl_Date.text = _Obj._created_at
        img_ImageView.fetchImage(_Obj._thumb_image)
    }
    
    func getFullDesc() {

        let url  = Constant.NewsURL + "/\(_Obj._id)"
        let auth = Constant.HeaderAuth

        self.showIndicator()
        MyApi.api.gettReqWithHeaderURLEnc(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionary ?? [:]
                let full_description = _data["full_description"]?.string ?? ""
                self.lbl_Desc.text = full_description
            }
        }
    }
    
    func getNov() {


        let url = Constant.NovNews + "\(_Obj._id)"
        let auth = Constant.HeaderAuth

        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                guard let res = response.dictionary else { return }
                let nov = res["nov"]?.int ?? 0

                print(nov)
                if nov > 0 {
                    self.lbl_ViewCount.text = "\(nov)"
                }else{
                    self.lbl_ViewCount.isHidden = true
                    self.view_Separator.isHidden = true
                    self.img_Nov.isHidden = true
                }
                
                self.getFullDesc()
            }

        }
    }

    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
