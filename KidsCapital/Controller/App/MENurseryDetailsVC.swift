//
//  MENurseryDetailsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/4/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase
import SwiftyJSON

class MENurseryDetailsVC: UIViewController {

    @IBOutlet weak var btn_ViewGallery: UIButton!

    @IBOutlet weak var btn_ShowMore: UIButton!
    @IBOutlet weak var stack_Social: UIStackView!
    @IBOutlet weak var img_Cover: UIImageView!
    @IBOutlet weak var view_Special: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_ShortDesc: UILabel!
    @IBOutlet weak var lbl_FullDesc: UILabel!

    @IBOutlet weak var lbl_Distance: UILabel!

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var btn_Site: UIButton!
    @IBOutlet weak var btn_Snapchat: UIButton!
    @IBOutlet weak var btn_Mobile: UIButton!
    @IBOutlet weak var btn_Whatsapp: UIButton!
    @IBOutlet weak var btn_Instagram: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    
    @IBOutlet weak var lbl_ViewCount: UILabel!
    @IBOutlet weak var view_Separator: UIView!
    @IBOutlet weak var img_Nov: UIImageView!

    @IBOutlet weak var view_Coupon: UIView!

    var _Obj : SearchResultST!
    var _NumberOfLines = 6
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "NURSERY".localized
        
//        Analytics.setScreenName("Nurseries details page", screenClass: "Nurseries details page")

        if Auth_User._Language == "ar" {
            btn_ViewGallery.imageEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        }
        
        lbl_ShortDesc.switchDirection()
        lbl_FullDesc.switchDirection()
        
        self.navigationItem.hideBackWord()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MENurseryDetailsCell")
        
        setInfo()
        getFacility()
        getNov()
    }
    
    func getNov() {
        
        let url = Constant.NovNurseries + "\(_Obj._id)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                guard let res = response.dictionary else { return }
                let nov = res["nov"]?.int ?? 0
                if nov > 0 {
                    self.lbl_ViewCount.text = "\(nov)"
                }else{
                    self.view_Separator.isHidden = true
                    self.lbl_ViewCount.isHidden = true
                    self.img_Nov.isHidden = true
                }
            }
        }
    }
    
    func setInfo() {
        
        img_Cover.fetchImage(_Obj._logo)
        view_Special.isHidden = _Obj._is_special ? false : true
        lbl_Title.text =  _Obj._name
        lbl_ShortDesc.text = _Obj._short_description
        lbl_Distance.text = _Obj._distance
        lbl_FullDesc.text = _Obj._full_description
        
        if lbl_FullDesc.calculateMaxLines() > _NumberOfLines {
            lbl_FullDesc.numberOfLines = _NumberOfLines
            self.btn_ShowMore.isHidden = false
        }
        
        let _screenSize = UIScreen.main.sizeType
        switch _screenSize {
        case .iPhone5:
            stack_Social.spacing = 2
            
        default:
            stack_Social.spacing = 10
        }
        
    }
   
    func getFacility() {
        
        let url = Constant.NurseriesURL + "/\(_Obj._id)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionary ?? [:]
                self._Obj = SearchResultST(_Obj: _data)
                self.SocialMediaExist()
                self.collectionView.reloadData()
                self.setInfo()
            }
        }
    }
    
    func SocialMediaExist() {
        
        self.btn_Site.isHidden = self._Obj._site == "" ? true : false
        self.btn_Snapchat.isHidden = self._Obj._snapchat == "" ? true : false
        self.btn_Whatsapp.isHidden = self._Obj._whatsapp == "" ? true : false
        self.btn_Instagram.isHidden = self._Obj._instagram == "" ? true : false
        
        self.btn_Mobile.isHidden = self._Obj._mobile == "" ? true : false
        self.btn_Share.isHidden  = self._Obj._share == "" ? true : false
    }
    
    
    @IBAction func SocialMediaAction(_ sender : UIButton) {
        
        
        switch sender {
        case btn_Site:
            OpenSocialMedia(link: self._Obj._site)
            
        case btn_Snapchat:
            let link = "https://www.snapchat.com/add/\(self._Obj._snapchat)"
            OpenSocialMedia(link: link)
            
        case btn_Mobile:
           let link = "tel://\(self._Obj._mobile)"
            OpenSocialMedia(link: link)
            
        case btn_Whatsapp:
            let link = "https://api.whatsapp.com/send?phone=\(self._Obj._whatsapp)"
            OpenSocialMedia(link: link)

        case btn_Instagram:
            let link = "instagram://user?username=\(self._Obj._instagram)"
            OpenSocialMedia(link: link)
            
        default:
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [self._Obj._share], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = (sender)
            activityViewController.popoverPresentationController?.permittedArrowDirections = .any
            self.present(activityViewController, animated: true)

        }
        
    }
    
    func OpenSocialMedia(link: String) {
        if let _url = URL(string: link) {
            if UIApplication.shared.canOpenURL(_url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(_url, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(_url)
                }
            }
            else {
                print("error")
            }
        }
    }
    
    @IBAction func ViewGallery(_ sender : UIButton) {
        let vc : MEGalleryCollectionVC = AppDelegate.MainSB.instanceVC()
        vc._id = "\(_Obj._id)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ShowMore(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.setImage("icon_ShowLess".toImage, for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.lbl_FullDesc.numberOfLines = self.lbl_FullDesc.calculateMaxLines()
                self.view.layoutIfNeeded()
            }
        }else{
            sender.setImage("icon_ShowMore".toImage, for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.lbl_FullDesc.numberOfLines = self._NumberOfLines
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension MENurseryDetailsVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _Obj._FacilitiesArray.count
    }
}

extension MENurseryDetailsVC : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MENurseryDetailsCell = collectionView.dequeueCVCell(indexPath: indexPath)
        let index = _Obj._FacilitiesArray[indexPath.row]
        
        cell.lbl_Facility.text = index._name
        cell.img_Facility.fetchImage(index._icon)
        
        return cell
    }
}

extension MENurseryDetailsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 90)
    }
}
