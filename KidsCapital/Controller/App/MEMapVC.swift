//
//  MEMapVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/4/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class MEMapVC: UIViewController , GMSMapViewDelegate , CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    
    var _resultArray : [SearchResultST] = []
    var locationManag = CLLocationManager()
    var flag = false
    var latit : Double?
    var long : Double?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "MAP".localized
        
        determineMyCurrentLocation()
    }
    
    func determineMyCurrentLocation() {
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.zoomGestures = true
        mapView.settings.scrollGestures = true
        mapView.delegate = self
        
        self.locationManag = CLLocationManager()
        self.locationManag.delegate = self
        self.locationManag.desiredAccuracy = kCLLocationAccuracyBest
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            self.isLocationEnable("Please enable location".localized)
            break
        }
    }
    
    func isLocationEnable(_ message : String) {
        self.showCustomAlert(okFlag: false, title: "", message: message, okTitle: "Ok", cancelTitle: "Cancel") { (action) in
            if action {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                }else { }
            }else{
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let Location : CLLocationCoordinate2D = (manager.location!.coordinate)
        let latitude = Location.latitude
        let longitude = Location.longitude
        latit = latitude
        long = longitude
        
        let camera = GMSCameraPosition.camera(withLatitude:CLLocationDegrees(latitude) , longitude:CLLocationDegrees(longitude) , zoom: 12)
        mapView.camera = camera
        mapView.animate(to: camera)
        
        let fullMarker = GMSMarker()
        fullMarker.position = CLLocationCoordinate2D(latitude: latit!, longitude:long!)
        fullMarker.icon = UIImage()
        fullMarker.map = self.mapView
        fullMarker.opacity = 1.0
        self.locationManag.stopUpdatingLocation()
        
        if !flag {
            self.MultiMarker()
            flag = true
        }
    }
    
    func MultiMarker() {
        for coord in _resultArray {
            let latitude = coord._lat
            let longitude = coord._lng
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
            marker.title = coord._name
            marker.icon = "icon_Marker".toImage
            marker.map = mapView
        }

    }
}
