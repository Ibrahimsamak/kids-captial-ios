//
//  MENewsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase

class MENewsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btn_ScrollToTop: UIButton!

    var _NewsArray : [NewsST] = []
    
    var _offestY : CGFloat = 0
    
    private var _current = 1
    private var _total = 0
    private var isLoading = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        Analytics.setScreenName("News list page", screenClass: "News list page")

        self.navigationItem.title = "NEWS".localized
        self.navigationItem.hideBackWord()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MENewsCell")
        
        getNews()
    }
    
    func getNews() {
        
        let url = Constant.NewsURL + "?page=\(_current)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                self._total = res["total"]?.int ?? 0
                let _dataArray = res["data"]?.array ?? []
                
                var tempArray : [NewsST] = []
                for _data in _dataArray {
                    let obj = NewsST(_Obj: _data)
                    tempArray.append(obj)
                }
                
                self._NewsArray.append(contentsOf: tempArray)
                self.tableView.reloadData()
                self.isLoading = false
            }
        }
    }
    
    @IBAction func ScrollToTopAction(_ sender : UIButton) {
        self.tableView.scrollToTop()
    }

}

extension MENewsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _NewsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _NewsArray[indexPath.row]
        let vc : MENewsDetailVC = AppDelegate.MainSB.instanceVC()
        vc._Obj = index
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MENewsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MENewsCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let index = _NewsArray[indexPath.row]
        
        cell.lbl_Date.text = index._created_at
        cell.lbl_Title.text = index._title
        cell.lbl_ShortDesc.text = index._short_description
        cell.img_Image.fetchImage(index._thumb_image)
        
        return cell
    }
}

extension MENewsVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y

        if scrollOffset <= 0 {
            self.btn_ScrollToTop.isHidden = true
            return
        }
        
        if (scrollOffset < _offestY) {
            self.btn_ScrollToTop.isHidden = false
            
        } else if (scrollOffset > _offestY) {
            self.btn_ScrollToTop.isHidden = true
            
        }
        
        if scrollOffset >= 0 {
            _offestY = scrollOffset
            
        } else if scrollOffset + scrollViewHeight == scrollContentSizeHeight {
            self.btn_ScrollToTop.isHidden = false
            
        }

        if scrollView == tableView {
            if ((scrollOffset + scrollViewHeight) >= (scrollContentSizeHeight))
            {
                if !isLoading {
                    if (self._NewsArray.count < self._total)
                    {
                        isLoading = true
                        self._current += 1
                        self.getNews()
                    }
                }
            }
        }
    }
}
