//
//  MEFilterResultVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/11/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit


class MEFilterResultVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout_TVHeight: NSLayoutConstraint!
    @IBOutlet weak var view_Coupon: UIView!
    
    
    var _TypeArray : [TypeST] = []
    var _Type = FilterType.none
    
    var _selectedIndex = -1
    var _selectedName = ""
    
    var delegate : ((FilterType,Int,String)->Void)?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ar = "FILTER".localized + " \(_Type.rawValue.localized)"
        let en = "\(_Type.rawValue.localized) " + "FILTER".localized
        
        title = Auth_User._Language == "ar" ? ar : en
            
        
        self.navigationItem.hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MESearchTypeCell")
        
        switch _Type {
        case .Area:
            
            if !DidOpenView._AreaOpened {
                DidOpenView._AreaOpened = true
                
                if Cached_Data._AreaArray.isEmpty {
                    self.showIndicator()
                }else{
                    appendArea()
                }
                
                getSearchType()
            }else{
                appendArea()
            }
            
        case .Category:
            
            if !DidOpenView._CategoryOpened {
                DidOpenView._CategoryOpened = true
                
                if Cached_Data._CategoryArray.isEmpty {
                    self.showIndicator()
                }else{
                    appendCategory()
                }
                
                getSearchType()
            }else{
                appendCategory()
            }
            
        default:
            
            if !DidOpenView._FacilitesOpened {
                DidOpenView._FacilitesOpened = true
                
                if Cached_Data._FacilitesArray.isEmpty {
                    self.showIndicator()
                }else{
                    appendFacility()
                }
                
                getSearchType()
            }else{
                appendFacility()
            }
            
        }        
    }
    
    func getSearchType() {
        
        let _time = _TypeArray.isEmpty ? 0 : _TypeArray.max {$0._timestamp < $1._timestamp}?._timestamp
        var url = ""
        switch self._Type {
        case .Area:
            url = Constant.AreaURL + "date=\(_time ?? 0)"

        case .Category:
            url = Constant.CategoryURL + "date=\(_time ?? 0)"

        default:
            url = Constant.FacilitiesURL + "date=\(_time ?? 0)"

        }
        
        let auth = Constant.HeaderAuth
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject ?? [:]}
                
                guard !ud_tempArray.isEmpty else {
                    return
                }

                switch self._Type {
                case .Area:
                    Cached_Data._AreaArray = self.OldItems(tempArray: ud_tempArray)
                    self.appendArea()
                    
                case .Category:
                    Cached_Data._CategoryArray = self.OldItems(tempArray: ud_tempArray)
                    self.appendCategory()
                    
                default:
                    Cached_Data._FacilitesArray = self.OldItems(tempArray: ud_tempArray)
                    self.appendFacility()
                    
                }

            }
        }
    }
    
    
    func OldItems(tempArray: [[String:Any]]) -> [[String:Any]] {
        
        var _oldItems : [[String:Any]] = [[:]]
        
        switch self._Type {
        case .Area:
            _oldItems = Cached_Data._AreaArray
        case .Category:
            _oldItems = Cached_Data._CategoryArray
        default:
            _oldItems = Cached_Data._FacilitesArray
        }

        if _oldItems.isEmpty {
            _oldItems = tempArray
        }else {
            tempArray.forEach({ (item) in
                let _id = item["id"] as? Int ?? 0
                let _changeType = item["changeType"] as? Int ?? 0
                
                let  index = _oldItems.index(where: { (item) -> Bool in
                    (item["id"] as! Int) == _id
                })
                
                if let _index = index {
                    switch _changeType {
                    case 2,1:
                        _oldItems[_index] = item
                    default:
                        _oldItems.remove(at: _index)
                    }
                }else {
                    if _changeType == 1 || _changeType == 2 {
                        _oldItems.append(item)
                    }
                }
            })
        }
        
        return _oldItems
    }
    
    func appendArea() {
        _TypeArray = Cached_Data._AreaArray.map({ (item) -> TypeST in
            return TypeST(_Obj: item)
        })
        self.layout_TVHeight.constant = CGFloat(self._TypeArray.count * 50)
        self.tableView.reloadData()
    }

    func appendCategory() {
        _TypeArray = Cached_Data._CategoryArray.map({ (item) -> TypeST in
            return TypeST(_Obj: item)
        })
        self.layout_TVHeight.constant = CGFloat(self._TypeArray.count * 50)
        self.tableView.reloadData()
    }
    
    func appendFacility() {
        _TypeArray = Cached_Data._FacilitesArray.map({ (item) -> TypeST in
            return TypeST(_Obj: item)
        })
        self.layout_TVHeight.constant = CGFloat(self._TypeArray.count * 50)
        self.tableView.reloadData()
    }
    
    @IBAction func DoneAction(_ sender : UIButton) {
        guard _selectedIndex != -1 else {
            switch self._Type {
            case .Area:
                self.showOkAlert(title: "", message: Auth_Strings.shared.AreaFilter())
                
            case .Category:
                self.showOkAlert(title: "", message: Auth_Strings.shared.ChooseCategory())
                
            default:
                self.showOkAlert(title: "", message: Auth_Strings.shared.AreaFacility())
                
            }
            return
        }
        
        self.delegate?(_Type, _selectedIndex, _selectedName)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension MEFilterResultVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _TypeArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }    
}

extension MEFilterResultVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MESearchTypeCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let index = _TypeArray[indexPath.row]
        cell.lbl_Title.text = index._name
        
        if index._id == _selectedIndex {
            cell.img_Checked.isHidden = false
        }else{
            cell.img_Checked.isHidden = true
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _TypeArray[indexPath.row]
        self._selectedIndex = index._id
        self._selectedName = index._name
        tableView.reloadData()
    }
}
