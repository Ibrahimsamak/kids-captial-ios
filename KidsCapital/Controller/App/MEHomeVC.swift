//
//  MEHomeVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/3/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation

var _latitude = ""
var _longitude = ""

class MEHomeVC: UIViewController , CLLocationManagerDelegate {

    @IBOutlet weak var btn_ChangeLang: UIBarButtonItem!

    @IBOutlet weak var btn_Provience: UIButton!
    @IBOutlet weak var btn_Category: UIButton!
    
    @IBOutlet weak var lbl_Province: UILabel!
    @IBOutlet weak var lbl_Category: UILabel!
    
    @IBOutlet weak var view_Coupon: UIView!

    var _ProvienceId = -1
    var _CategoryId = -1

    var _ProvienceName = ""
    var _CategoryName = ""

    var locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.hideBackWord()
        self.navigationItem.title = "HOME".localized
//        Analytics.setScreenName("Home Page", screenClass: "Home page")
    
        btn_ChangeLang.isEnabled = false
        btn_ChangeLang.image = UIImage()
        
        setDefault()
        determineMyCurrentLocation()
    }

    func setDefault() {
        _ProvienceId = Auth_User._ProvinceId
        _ProvienceName = Auth_User._provinceName
        
        _CategoryId = Auth_User._CategoryId
        _CategoryName = Auth_User._CategoryName
        
        lbl_Province.text = Auth_User._provinceName == "" ? "Province".localized : Auth_User._provinceName
        lbl_Category.text = Auth_User._CategoryName == "" ? "Category".localized : Auth_User._CategoryName
    }

    func determineMyCurrentLocation() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse, .authorizedAlways:
            if let Location : CLLocationCoordinate2D = manager.location?.coordinate {
                _latitude = "\(Double(Location.latitude))"
                _longitude = "\(Double(Location.longitude))"
            }

            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            _latitude = ""
            _longitude = ""
//            self.isLocationEnable("الرجاء تفعيل خاصية الوصول الى موقعك من اجل عرض المغاسل القريبة")
            break
        }
    }
    
    @IBAction func SearchTypeAction(_ sender : UIButton) {
        
        let vc : MESearchTypeVC = AppDelegate.MainSB.instanceVC()
        vc.delegate = { (isProvience, _id, _name)in
            if isProvience {
                self._ProvienceId = _id
                self._ProvienceName = _name
                
                Auth_User._ProvinceId = _id
                Auth_User._provinceName = _name
                
                self.lbl_Province.text = _name
            }else{
                self._CategoryId = _id
                self._CategoryName = _name
                
                Auth_User._CategoryId = _id
                Auth_User._CategoryName = _name
                self.lbl_Category.text = _name
            }
        }
        
        switch sender {
        case btn_Provience:
            vc._selectedName = _ProvienceName
            vc._selectedIndex = _ProvienceId
            vc._IsProvience = true
        default:
            vc._selectedName = _CategoryName
            vc._selectedIndex = _CategoryId
            vc._IsProvience = false
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func SearchAction(_ sender : UIButton) {
        
//        guard _ProvienceName != "" || _CategoryName != "" else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.ChooseSearchType())
//            return
//        }
        
        guard _ProvienceId != -1 else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ProvienceRequired())
            return
        }
        
        guard _CategoryId != -1 else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.CategoryRequired())
            return
        }
        
        let vc : MEListViewVC = AppDelegate.MainSB.instanceVC()
        vc._provienceId = self._ProvienceId
        vc._categoryId = self._CategoryId
        vc._categoryName = self._CategoryName
        vc._provienceName = self._ProvienceName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ChangeLang(_ sender: UIBarButtonItem) {
        
        if Auth_User._Language == "en" {
            Language.setAppLanguage(lang: "ar")
            Auth_User._Language = "ar"
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            Language.setAppLanguage(lang: "en-US")
            Auth_User._Language = "en"
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        resetConfig()
        changeHeaderLang()
        reload_App()
    }
    
    func resetConfig() {
        
        DidOpenView._AdsCategoriesOpened = false
        DidOpenView._FacilitesOpened = false
        DidOpenView._CategoryOpened = false
        DidOpenView._ProvinceOpened = false
        DidOpenView._AreaOpened = false
        
        Cached_Data._ProvienceArray = []
        Cached_Data._AreaArray = []
        Cached_Data._CategoryArray = []
        Cached_Data._FacilitesArray = []
        Cached_Data._AdsCategories = []
        Cached_Data._PagesArray = []
        Cached_Data._ContactUsObject = [:]
        
        
        Auth_User._CategoryId = -1
        Auth_User._ProvinceId = -1
        Auth_User._CategoryName = ""
        Auth_User._provinceName = ""
    }
    
    func changeHeaderLang() {
        Constant.HeaderAuth.updateValue(Auth_User._Language, forKey: "Accept-Language")
        Constant.HeaderTokenLang.updateValue(Auth_User._Language, forKey: "Accept-Language")
    }
    
    func reload_App() {
        let vc : METabbarVC = AppDelegate.MainSB.instanceVC()
        self.RootViewController(viewController: vc)
    }
}
