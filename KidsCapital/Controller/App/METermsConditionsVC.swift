//
//  METermsConditionsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class METermsConditionsVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var lbl_Title: UILabel!
//    @IBOutlet weak var lbl_Desc: UILabel!
    @IBOutlet weak var view_Coupon: UIView!

    var Obj : PageST!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = Obj._title
        
        lbl_Title.switchDirection()
//        lbl_Desc.switchDirection()
        
        lbl_Title.text = Obj._title
        getDesc()
    }
    
    func getDesc() {
        
        let url = Constant.PagesURL + "/\(Obj._id)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let details = res["data"]?["details"].string ?? ""
                
                self.configWebView(content: details)
            }
        }
    }
    
    func configWebView(content: String) {
        webView.loadHTMLString(content, baseURL: nil)
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
