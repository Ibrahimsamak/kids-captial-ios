//
//  MEAdsCategoriesVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/6/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase

class MEAdsCategoriesVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var _AdsCateArray : [AdsCategoryST] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !DidOpenView._AdsCategoriesOpened {
            DidOpenView._AdsCategoriesOpened = true
            
            if Cached_Data._AdsCategories.isEmpty {
                self.showIndicator()
            }else{
                appendAdsCate()
            }
            
            getAdsCategroy()
            
        }else{
            appendAdsCate()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ADS CATEGORIES".localized
        self.navigationItem.hideBackWord()
//        Analytics.setScreenName("Ads category page", screenClass: "Ads category page")

        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MEAdsCategoriesCell")

    }
    
    func getAdsCategroy() {
        
        let _time = _AdsCateArray.isEmpty ? 0 : _AdsCateArray.max {$0._timestamp < $1._timestamp}?._timestamp
        let url  = Constant.AdsCategoriesURL + "date=\(_time ?? 0)"
        let auth = Constant.HeaderAuth

        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject!}
                var _oldItems = Cached_Data._AdsCategories
                
                guard !ud_tempArray.isEmpty else {
                    return
                }

                if _oldItems.isEmpty {
                    Cached_Data._AdsCategories = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._AdsCategories = _oldItems
                }

                self.appendAdsCate()
            }
        }
    }
    
    func appendAdsCate() {
        _AdsCateArray = Cached_Data._AdsCategories.map({ (item) -> AdsCategoryST in
            return AdsCategoryST(_Obj: item)
        })
        self.collectionView.reloadData()
    }
}

extension MEAdsCategoriesVC: UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _AdsCateArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = _AdsCateArray[indexPath.row]
        let vc : MEAdsListVC = AppDelegate.MainSB.instanceVC()
        vc._cateId = "\(index._id)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width)
        return CGSize(width: ((width / 2) - 40), height: 220)
    }
}

extension MEAdsCategoriesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MEAdsCategoriesCell = collectionView.dequeueCVCell(indexPath: indexPath)
        
        let index = _AdsCateArray[indexPath.row]
        cell.lbl_Title.text = index._name
        cell.img_Ads.fetchImage(index._icon)
        
        return cell
    }
}
