//
//  MEListViewVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/3/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import Firebase

class MEListViewVC: UIViewController , CLLocationManagerDelegate {
    
    @IBOutlet weak var view_NoResult: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout_TVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btn_Map: UIButton!
    @IBOutlet weak var btn_Filter: UIButton!
    
    @IBOutlet weak var btn_ScrollToTop: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    var _resultArray : [SearchResultST] = []
    let cell_Height = 178
    var _provienceId = -1
    var _categoryId = -1

    private var _current = 1
    private var _total = 0
    private var isLoading = true
    private var isFilter = false
    
    var _provienceName = ""
    var _categoryName = ""
    
    var _originalTitle = ""
    var _filterTitle = ""
    
    var _originalUrl = "?lat=\(_latitude)" + "&lng=\(_longitude)"
    var _filterURL = "?lat=\(_latitude)" + "&lng=\(_longitude)"
    
    var _offestY : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        Analytics.setScreenName("Nurseries list page", screenClass: "Nurseries list page")

        if Auth_User._Language == "ar" {
            btn_Map.imageEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 10)
            btn_Filter.imageEdgeInsets = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 10)
        }
        
        if _provienceName == "" {
            _originalTitle = "\(_categoryName) ,\(_categoryId)"
            _originalUrl += "&category_id=\(_categoryId)"
            
        }else if _categoryName == "" {
            _originalTitle = "\(_provienceName) ,\(_provienceId)"
            _originalUrl += "&province_id=\(_provienceId)"
            
        }else{
            _originalTitle = "\(_provienceName),\(_categoryName) ,\(_provienceId),\(_categoryId)"
            _originalUrl += "&category_id=\(_categoryId)" + "&province_id=\(_provienceId)"

        }
        
        title = _originalTitle
        
        scrollView.delegate = self
        self.navigationItem.hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MEListViewCell")

        btn_Map.setRounded(radius: 3)
        btn_Filter.setRounded(radius: 3)
        
        getSearchResult(_originalUrl)
    }
    
    func getSearchResult(_ url: String) {
        
        let url  = Constant.NurseriesURL + url + "&page=\(_current)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                self._total = res["total"]?.int ?? 0
                let _dataArray = res["data"]?.array ?? []
             
                var tempArray : [SearchResultST] = []
                for _data in _dataArray {
                    let _obj = SearchResultST(_Obj: _data)
                    tempArray.append(_obj)
                }
                
                self._resultArray.append(contentsOf: tempArray)
                
                if self._resultArray.isEmpty {
                    self.btn_ScrollToTop.isHidden = true
                    UIView.animate(withDuration: 0.3, animations: {
                        self.view_NoResult.isHidden = false
                        self.view.layoutIfNeeded()
                    })
                }else{
                    self.view_NoResult.isHidden = true
                }

                let _title = self.isFilter ? self._filterTitle : self._originalTitle
                self.title = _title + " | \(self._resultArray.self.count) " + "results".localized
                self.layout_TVHeight.constant = CGFloat(self._resultArray.count * self.cell_Height)
                self.tableView.reloadData()
                self.isLoading = false
            }
        }
        
    }
    
    @IBAction func MultiAction(_ sender : UIButton) {
        switch sender {
        case btn_Map:
            let vc : MEMapVC = AppDelegate.MainSB.instanceVC()
            vc._resultArray = self._resultArray
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            let vc : MEFilterVC = AppDelegate.MainSB.instanceVC()
            vc.delegate = { (url,titles) in
                self._resultArray = []
                self._current = 1
                if url == "" {
                    self.isFilter = false
                    self.title = self._originalTitle
                    self.getSearchResult(self._originalUrl)
                }else{
                    self.isFilter = true
                    self.title = titles
                    self._filterTitle = titles
                    self.getSearchResult(self._filterURL + url)
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func ModifySearch(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ScrollToTopAction(_ sender : UIButton) {
        self.scrollView.scrollToTop()
    }

}

extension MEListViewVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _resultArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(cell_Height)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _resultArray[indexPath.row]
        let vc : MENurseryDetailsVC = AppDelegate.MainSB.instanceVC()
        vc._Obj = index
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MEListViewVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MEListViewCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let index = _resultArray[indexPath.row]
        
        cell._FacilityArray = index._FacilitiesArray
        
        if index._is_special {
            cell.view_Special.isHidden = false
            cell.img_SpecialFlag.isHidden = false
        }else{
            cell.view_Special.isHidden = true
            cell.img_SpecialFlag.isHidden = true
        }
        
        // Check Location
        cell.view_OpenReg.isHidden = index._open_reg == 1 ? false : true
        cell.lbl_Title.text = index._name
        cell.lbl_Content.text = index._short_description
        cell.lbl_Distance.text = index._distance
        cell.img_Logo.fetchImage(index._logo)
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y

        if scrollOffset <= 0 {
            self.btn_ScrollToTop.isHidden = true
            return
        }
        
        if (scrollOffset < _offestY) {
            self.btn_ScrollToTop.isHidden = false
            
        } else if (scrollOffset > _offestY) {
            self.btn_ScrollToTop.isHidden = true
            
        }
        
        if scrollOffset >= 0 {
            _offestY = scrollOffset
            
        } else if scrollOffset + scrollViewHeight == scrollContentSizeHeight {
            self.btn_ScrollToTop.isHidden = false
            
        }
        
        
        if ((scrollOffset + scrollViewHeight) >= (scrollContentSizeHeight))
        {
            if !isLoading {
                if (self._resultArray.count < self._total)
                {
                    isLoading = true
                    self._current += 1
                    let url = isFilter ? _filterURL : _originalUrl
                    self.getSearchResult(url)
                }
            }
        }
    }
}
