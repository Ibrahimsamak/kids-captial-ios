//
//  MEGalleryCollectionVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEGalleryCollectionVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var view_Coupon: UIView!

    var _id = ""
    var _ImagesArray : [GalleryST] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "GALLERY".localized
        self.navigationItem.hideBackWord()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MEGalleryCollectionCell")
        
        CollectionLayout()
        getGallery()
    }
    
    func CollectionLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 30)
        layout.itemSize = CGSize(width: ((width / 2) - 40), height: 220)
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        collectionView!.collectionViewLayout = layout
    }

    func getGallery() {
        
        let url = Constant.GalleriesURL + _id
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                for _data in _dataArray {
                    let obj = GalleryST(_Obj: _data)
                    self._ImagesArray.append(obj)
                }
                
                self.collectionView.reloadData()
            }
        }
        
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension MEGalleryCollectionVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _ImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = _ImagesArray[indexPath.item]
        let vc : MEGalleryImagesVC = AppDelegate.MainSB.instanceVC()
        vc._ImagesArray = index._ImagesArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MEGalleryCollectionVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MEGalleryCollectionCell = collectionView.dequeueCVCell(indexPath: indexPath)
        let index = _ImagesArray[indexPath.item]
        
//        cell._Images = index._ImagesArray
        cell.lbl_Title.text = index._name
        cell.img_Main.fetchImage(index._main_image)
        
        return cell
    }
}

