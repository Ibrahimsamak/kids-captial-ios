//
//  MEClaimCouponVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/16/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEClaimCouponVC: UIViewController {

    @IBOutlet weak var txt_Title: UILabel!

    @IBOutlet weak var txt_Id: UITextField!
    @IBOutlet weak var txt_Coupons: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "COUPONS"
        txt_Id.isUserInteractionEnabled = false
        txt_Id.switchDirection()
        txt_Coupons.switchDirection()
        txt_Title.switchDirection()
        
    }
    
    @IBAction func ConfirmAction(_ sender : UIButton) {
        
//        guard let _Id = txt_Id.text?.TrimWhiteSpaces, !_Id.isEmpty else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.NurseryId())
//            return
//        }
        
        guard let _Code = txt_Coupons.text?.TrimWhiteSpaces, !_Code.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.CouponCode())
            return
        }
     
        let url = Constant.MemberClaimURL
        let auth = Constant.HeaderTokenLang
        let param = ["code" : _Code]
        
        self.showIndicator()
        self.view.endEditing(true)
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let errors = res["errors"]?.dictionaryObject ?? [:]
                let message = res["message"]?.string ?? ""

                if errors.isEmpty {
                    self.showOkAlertWithComp(title: "", message: message, completion: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
    }
    
    
}
