//
//  MEEditAdVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/8/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Photos
import TLPhotoPicker
import SwiftyPickerPopover
import IQKeyboardManagerSwift

class MEEditAdVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txt_Title: UITextField!
    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_Category: UITextField!
    @IBOutlet weak var txt_Price: UITextField!
    @IBOutlet weak var txt_Desc: IQTextView!

    @IBOutlet var lbl_Collection: [UILabel]!

    var _AdsObj : AdsDetailST!
    var _AdsCateArray : [AdsCategoryST] = []
    
    var selectedAssets = [TLPHAsset]()
    private var imageArray : [ImageST] = []
    private var deletedImage : [Int] = []

    var _Id = -1
    
    var delegate : (()->Void)?
    var _userInfo : UserST!
    let timeStamp = Date().toMillis()!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "EDIT AD"
        
        switchDirection()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MENewEditAdCell")
        collectionView.registerCell(id: "MEFirstCell")
        self._userInfo = UserST(Auth_User.ـUserInfo)

        txt_Desc.placeholder = "Description".localized
        self._Id = _AdsObj._category_id
        
        if !DidOpenView._AdsCategoriesOpened {
            DidOpenView._AdsCategoriesOpened = true
            
            if Cached_Data._AdsCategories.isEmpty {
                self.showIndicator()
            }else{
                appendAdsCate()
            }
            
            getAdsCategroy()
            
        }else{
            appendAdsCate()
        }
        
        setInfo()
    }

    func switchDirection() {
        for lbl in lbl_Collection {
            lbl.switchDirection()
        }
        
        txt_Title.switchDirection()
        txt_Phone.switchDirection()
        txt_Category.switchDirection()
        txt_Price.switchDirection()
        txt_Desc.switchDirection()
    }

    func getAdsCategroy() {
        
        let _time = _AdsCateArray.isEmpty ? 0 : _AdsCateArray.max {$0._timestamp < $1._timestamp}?._timestamp
        let url  = Constant.AdsCategoriesURL + "date=\(_time ?? 0)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }
                
                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject!}
                var _oldItems = Cached_Data._AdsCategories
                
                guard !ud_tempArray.isEmpty else {
                    return
                }
                
                if _oldItems.isEmpty {
                    Cached_Data._AdsCategories = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._AdsCategories = _oldItems
                }
                
                self.appendAdsCate()
            }
        }
    }
    
    func appendAdsCate() {
        _AdsCateArray = Cached_Data._AdsCategories.map({ (item) -> AdsCategoryST in
            return AdsCategoryST(_Obj: item)
        })

        _AdsCateArray.forEach { (item) in
            if item._id == self._Id {
                self.txt_Category.text = item._name
            }
        }
        
        self.collectionView.reloadData()
    }

    
    func setInfo() {
        txt_Title.text = _AdsObj._title
        txt_Phone.text = _AdsObj._phone
        txt_Price.text = "\(_AdsObj._price)"
        txt_Desc.text  = _AdsObj._details
        
        imageArray  = _AdsObj._ImagesArray.map({ (item) -> ImageST in
            return ImageST(id: item._id, url: item._name)
        })
    }
    
    @IBAction func PickCategory(_ sender : UIButton) {
        let _name = _AdsCateArray.map {$0._name}
        StringPickerPopover(title: "Category", choices: _name)
            .setSelectedRow(0)
            .setFont(UIFont.systemFont(ofSize: 18 , weight: .regular))
            .setDoneButton(color: UIColor.white,action: { (popover, selectedRow, selectedString) in
                self._Id = self._AdsCateArray[selectedRow]._id
                self.txt_Category.text = selectedString
            })
            .setCancelButton(color: UIColor.white, action: { (_, _, _) in print("cancel")}
            )
            .appear(originView: sender, baseViewController: self)
    }

    @IBAction func EditAction(_ sender : UIButton) {
        
        guard let _Title = txt_Title.text?.TrimWhiteSpaces, !_Title.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.TitleRequired())
            return
        }
        
        guard let Phone = txt_Phone.text?.TrimWhiteSpaces, !Phone.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PhoneRequired())
            return
        }
        
        if self._Id == -1 {
            self.showOkAlert(title: "", message: Auth_Strings.shared.CategoryRequired())
            return
        }
        
        guard let Price = txt_Price.text?.TrimWhiteSpaces, !Price.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PriceRequired())
            return
        }
        
        guard let Desc = txt_Desc.text?.TrimWhiteSpaces, !Desc.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.DescRequired())
            return
        }
        
        let url  = Constant.AdsListURL + "/\(self._AdsObj._id)"
        let auth = Constant.HeaderUpdate
        
        let param = ["title" : _Title,
                     "details" : Desc,
                     "price" : Price,
                     "ads_category_id" : _Id,
                     "email" : _userInfo._email,
                     "mobile" : Phone,
                     "deleted[]" : self.deletedImage] as [String:Any]
        
        self.view.endEditing(true)
        self.showIndicator()
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                guard let res = response.dictionary else { return }
//                let _data = res["data"]?.dictionary ?? [:]
//                let message = res["message"]?.string ?? ""
                let errors = res["errors"]?.dictionaryObject ?? [:]
                
                self.imageArray = self.imageArray.filter {$0._id == -1}
                let images_car = self.imageArray.map {return $0._image}

                if errors.isEmpty {
                    
                    if images_car.isEmpty {
                        self.hideIndicator()
                        self.showOkAlertWithComp(title: "", message: "Successfully Editied".localized, completion: { (_) in
                            self.delegate?()
                        })
                        
                    }else{
                        self.UploadImages(imageArray: images_car, _id: self._AdsObj._id)
                    }

                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
                
            }
        }
    }

    func UploadImages(imageArray: [UIImage],_id: Int) {

        let url = Constant.AdsListURL + "/\(_id)" + "/addImage"
        let auth = Constant.HeaderToken

        var count = 0
        for img in imageArray {
            MyApi.api.upload_image_multipart(url: url, header: auth, p1_image: img) { (error, response) in
                if error != nil {
                    self.hideIndicator()
                    self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                    return
                }else{
                    count += 1
                    guard let res = response.dictionary else { return }
                    print(res)

                    if (self.imageArray.count) == count {
                        self.hideIndicator()
                        self.showOkAlertWithComp(title: "", message: "Successfully Editied", completion: { (_) in
                            self.delegate?()
                        })
                    }
                }
            }
        }
    }
    
}

extension MEEditAdVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (imageArray.count) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        if index == 0 {
            self.PickPhotos()
        }
    }
    
    func PickPhotos() {
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        
        var configure = TLPhotosPickerConfigure()
        configure.allowedVideo = false
        configure.allowedLivePhotos = false
        configure.allowedVideoRecording = false
        configure.mediaType = .image
        
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.configure = configure
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        self.present(viewController, animated: true, completion: nil)
    }
    
    
}

extension MEEditAdVC : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            let cell: MEFirstCell = collectionView.dequeueCVCell(indexPath: indexPath)
            
            return cell
        }else{
            let cell : MENewEditAdCell = collectionView.dequeueCVCell(indexPath: indexPath)
            let _item = self.imageArray[indexPath.row - 1]
            
            switch _item._id {
            case -1:
                cell.img_Image.image = _item._image
            default:
                cell.img_Image.fetchImage(_item._url)
            }

            cell.btn_Remove.isHidden = false
            cell.btn_Remove.tag = indexPath.item
            cell.btn_Remove.addTarget(self, action: #selector(RemoveImage), for: .touchUpInside)
            
            return cell
        }
    }
    
    @objc func RemoveImage(_ sender : UIButton) {
        let item = self.imageArray[sender.tag - 1]
        self.imageArray.remove(at: sender.tag - 1)
        self.collectionView.reloadData()
        guard item._id != -1 else {return}
        self.deletedImage.append(item._id)
    }
    
    
}

extension MEEditAdVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 100)
    }
}

extension MEEditAdVC : TLPhotosPickerViewControllerDelegate {
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear

//        self.imageArray = []
        for img in withTLPHAssets {
            let conv = getAssetThumbnail(asset: img.phAsset!)
            self.imageArray.append(ImageST.init(id: -1, image: conv))
        }

        self.selectedAssets = withTLPHAssets
        self.collectionView.reloadData()
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 500, height: 500), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    func photoPickerDidCancel() {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    func dismissComplete() { }
    
    func canSelectAsset(phAsset: PHAsset) -> Bool { return true }
    
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) { }
    
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) { }
    
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) { }
    
//    func getUIImage(asset: PHAsset) -> UIImage {
//
//        var img = UIImage()
//        let manager = PHImageManager.default()
//        let options = PHImageRequestOptions()
//        options.version = .original
//        options.isSynchronous = true
//        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
//
//            if let data = data {
//                img = UIImage(data: data) ?? UIImage()
//            }
//        }
//        return img
//    }
    
}
