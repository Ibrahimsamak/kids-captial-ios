//
//  MECoupansDeatilsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/6/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MECoupansDeatilsVC: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_EndDate: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    @IBOutlet weak var lbl_titleDesc: UILabel!

    @IBOutlet weak var btn_Claim: UIButton!
    
    var _obj : CouponST!
    var _myCoupon = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "COUPONS DETAILS".localized
        lbl_titleDesc.switchDirection()
        lbl_Desc.switchDirection()
        
        btn_Claim.isEnabled = _myCoupon ? false : true
        
        setInfo()
    }
    
    
    @IBAction func ClaimCoupon(_ sender : UIButton) {
        
        let url = Constant.ClaimCouponsURL + "\(_obj._id)"
        let auth = Constant.HeaderTokenLang
        let param = ["code" : _obj._code]
        
        self.showIndicator()
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let message = res["message"]?.string ?? ""
                let errors = res["errors"]?.dictionaryObject ?? [:]
                let coupon_code = res["coupon_code"]?.string ?? ""
                
                if errors.isEmpty {
                    if coupon_code == "" {
                        self.showOkAlert(title: "", message: message)
                    }else{
                        self.lbl_Code.text = coupon_code
                        let vc : MESuccessfullyVC = AppDelegate.MainSB.instanceVC()
                        vc.modalTransitionStyle = .crossDissolve
                        vc.modalPresentationStyle = .custom
                        self.present(vc, animated: true)
                    }
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
    }
    
    func setInfo() {
        lbl_Title.text = _obj._title
        lbl_EndDate.text = _obj._end_date
        lbl_Code.text = _myCoupon ? _obj._coupon_code : "Claim Now".localized
        lbl_Desc.text = _obj._description
    }
    
}
