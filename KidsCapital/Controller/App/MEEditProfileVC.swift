//
//  MEEditProfileVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/16/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEEditProfileVC: UIViewController {

    @IBOutlet weak var txt_Mutual: UITextField!
    
    var _type = EditProfile.none
    var _userInfo : UserST!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = _type.rawValue.localized
        txt_Mutual.switchDirection()
        _userInfo = UserST(Auth_User.ـUserInfo)
        
        switch _type {
        case .Phone:
            txt_Mutual.text = _userInfo._mobile
            txt_Mutual.placeholder = "Phone number"
            txt_Mutual.keyboardType = .asciiCapableNumberPad
            
        case .Name:
            txt_Mutual.text = _userInfo._name
            txt_Mutual.placeholder = "Fullname"
            txt_Mutual.keyboardType = .default
            
        case .Username:
            txt_Mutual.text = _userInfo._username
            txt_Mutual.placeholder = "Username"

        default:
            print("none")
        }
    }
    
    @IBAction func SaveAction(_ sender: UIButton) {
        
        
        guard let textField = txt_Mutual.text?.TrimWhiteSpaces, !textField.isEmpty else {
            let msg = _type == .Name ? Auth_Strings.shared.FullnameRequired() : Auth_Strings.shared.PhoneRequired()
            self.showOkAlert(title: "", message: msg)
            return
        }
        
        var key = ""
        
        switch _type {
        case .Name:
            key = "name"
            
        case .Phone:
            key = "mobile"
            
        case .Username:
            key = "username"
            
        default:
            break
        }
        
        let url = Constant.EidtProfileURL
        let auth = Constant.HeaderTokenLang
        let param = [key : textField]
        
        self.showIndicator()
        MyApi.api.putRequestWihtoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionaryObject ?? [:]
                let errors = res["errors"]?.dictionaryObject ?? [:]
                
                if errors.isEmpty {
                    Auth_User.ـUserInfo = _data
                    self.showOkAlertWithComp(title: "", message: "Successfully Changed".localized, completion: { (_) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }
    }
    
}
