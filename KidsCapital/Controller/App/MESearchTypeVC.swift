//
//  MESearchTypeVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/3/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase

class MESearchTypeVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout_TVHeight: NSLayoutConstraint!
    @IBOutlet weak var view_Coupon: UIView!
    
    var _IsProvience = false
    var _TypeArray : [TypeST] = []
    
    var delegate : ((Bool,Int,String)->Void)?
    
    var _selectedIndex = -1
    var _selectedName = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }
    
    let notificationCenter = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = _IsProvience ? "SELECT PROVINCE".localized : "SELECT CATEGORY".localized
        self.navigationItem.hideBackWord()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MESearchTypeCell")
        
        if _IsProvience {
            if !DidOpenView._ProvinceOpened {
                DidOpenView._ProvinceOpened = true
                
                if Cached_Data._ProvienceArray.isEmpty {
                    self.showIndicator()
                }else{
                    appendProvience()
                }
                getProvience()
                
            }else{
                appendProvience()
            }
            
        }else{
            
            if !DidOpenView._CategoryOpened {
                DidOpenView._CategoryOpened = true
                
                if Cached_Data._CategoryArray.isEmpty {
                    self.showIndicator()
                }else{
                    appendCategory()
                }
                
                getCategory()
                
            }else{
                appendCategory()
            }
            
        }
    }
    
    func getProvience() {
        
        let _time = _TypeArray.isEmpty ? 0 : _TypeArray.max {$0._timestamp < $1._timestamp}?._timestamp
        let url = Constant.ProvincesURL + "date=\(_time ?? 0)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject ?? [:] }
                var _oldItems = Cached_Data._ProvienceArray

                guard !ud_tempArray.isEmpty else {
                    return
                }

                if _oldItems.isEmpty {
                    Cached_Data._ProvienceArray = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._ProvienceArray = _oldItems
                }
                
                self.appendProvience()
            }
        }
    }
    
    func appendProvience() {
        _TypeArray = Cached_Data._ProvienceArray.map({ (item) -> TypeST in
            return TypeST(_Obj: item)
        })
        _TypeArray = _TypeArray.filter {$0._parent_id == 0}
        self.layout_TVHeight.constant = CGFloat(self._TypeArray.count * 50)
        self.tableView.reloadData()
    }
    
    func getCategory() {
        
        let _time = _TypeArray.max {$0._timestamp < $1._timestamp}?._timestamp
        let url  = Constant.CategoryURL + "date=\(_time ?? 0)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject ?? [:]}
                var _oldItems = Cached_Data._CategoryArray
                
                guard !ud_tempArray.isEmpty else {
                    return
                }
                
                if _oldItems.isEmpty {
                    Cached_Data._CategoryArray = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._CategoryArray = _oldItems
                }
                
                self.appendCategory()
            }
        }
    }
    
    func appendCategory() {
        _TypeArray = Cached_Data._CategoryArray.map({ (item) -> TypeST in
            return TypeST(_Obj: item)
        })
        self.layout_TVHeight.constant = CGFloat(self._TypeArray.count * 50)
        tableView.reloadData()
    }
    
    @IBAction func DoneAction(_ sender : UIButton) {
        guard _selectedIndex != -1 else {
            let msg = _IsProvience ? Auth_Strings.shared.ChooseProvience() : Auth_Strings.shared.ChooseCategory()
            self.showOkAlert(title: "", message: msg)
            return
        }
        
        self.delegate?(_IsProvience,_selectedIndex, _selectedName)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension MESearchTypeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _TypeArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension MESearchTypeVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MESearchTypeCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let index = _TypeArray[indexPath.row]
        cell.lbl_Title.text = index._name
        
        if index._id == _selectedIndex {
            cell.img_Checked.isHidden = false
        }else{
            cell.img_Checked.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _TypeArray[indexPath.row]
        self._selectedIndex = index._id
        self._selectedName = index._name
        tableView.reloadData()
    }
}
