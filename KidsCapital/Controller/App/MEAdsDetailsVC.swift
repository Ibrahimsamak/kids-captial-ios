//
//  MEAdsDetailsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/7/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import MessageUI
import ImageSlideshow

class MEAdsDetailsVC: UIViewController  , MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var stack_Nov: UIStackView!
    @IBOutlet weak var layout_SliderHeight: NSLayoutConstraint!
    @IBOutlet weak var layout_StackHeight: NSLayoutConstraint!
    @IBOutlet weak var layout_ShowMoreHeight: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_ViewCount: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!

    @IBOutlet weak var btn_Mail: UIButton!
    @IBOutlet weak var btn_Call: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_ShowMore: UIButton!

    @IBOutlet weak var stack_Buttons: UIStackView!
    @IBOutlet weak var imageSlider: ImageSlideshow!
    
    var _AdsObj : AdsDetailST?
    var _adsId = ""
    var _isMyAds = false
    var _NumberOfLines = 6

    var delegate : (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "AD DETAILS".localized
        self.navigationItem.hideBackWord()
//        Analytics.setScreenName("Ad details page", screenClass: "Ad details page")

        lbl_Content.switchDirection()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        imageSlider.addGestureRecognizer(gestureRecognizer)

        if _isMyAds {
            stack_Buttons.isHidden = false
            layout_StackHeight.constant = 56
        }
        
        getNov()
        getAdsDetails()
    }
    
    @objc func didTap() {
        imageSlider.presentFullScreenController(from: self)
    }

    func getNov() {
        
        let url = Constant.NovAds + _adsId
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                guard let res = response.dictionary else { return }
                let nov = res["nov"]?.int ?? 0
                if nov > 0 {
                    self.lbl_ViewCount.text = "\(nov)"
                }else{
                    self.stack_Nov.isHidden = true
                }
            }
        }
    }
    
    func getAdsDetails() {
        
        let url = Constant.AdsListURL + "/\(_adsId)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionary ?? [:]
                self._AdsObj = AdsDetailST(_Obj: _data)
                
                self.setInfo()
            }
        }
    }
 
    func setInfo() {
        let obj = self._AdsObj
        
        lbl_Date.text = obj?._created_at
        lbl_Price.text = "KD \(obj?._price ?? 0)"
        lbl_Title.text = obj?._title
        lbl_Content.text = obj?._details
        
        if lbl_Content.calculateMaxLines() > _NumberOfLines {
            lbl_Content.numberOfLines = _NumberOfLines
//            self.btn_ShowMore.isHidden = false
            self.layout_ShowMoreHeight.constant = 25
        }

        if !obj!._ImagesArray.isEmpty {
            UIView.animate(withDuration: 0.4) {
                self.layout_SliderHeight.constant = 270
                self.imageSlider.isHidden = false
                self.view.layoutIfNeeded()
            }
        }else{
            UIView.animate(withDuration: 0.4) {
                self.layout_SliderHeight.constant = 0
                self.imageSlider.isHidden = true
                self.view.layoutIfNeeded()
            }
        }
        
        self.setupImageSlider()
    }
    
    func setupImageSlider() {
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = .black
        pageIndicator.pageIndicatorTintColor = "FFDD00".color

        let arr =  self._AdsObj?._ImagesArray.map { KingfisherSource(urlString: $0._name)}
        imageSlider.pageIndicator = pageIndicator
        imageSlider.contentScaleMode = .scaleAspectFill
        imageSlider.clipsToBounds = true
        imageSlider.slideshowInterval = 4.0
        imageSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        imageSlider.backgroundColor = .clear
        imageSlider.scrollView.setRounded(radius: 4)
        imageSlider.setImageInputs(arr as? [InputSource] ?? [])
    }
    
    @IBAction func SocialAction(_ sender : UIButton) {
        switch sender {
        case btn_Mail:
            guard !(self._AdsObj?._email.isEmpty)! else {
                self.showOkAlert(title: "", message: Auth_Strings.shared.NoEmail())
                return
            }
            sendEmail(email: self._AdsObj?._email ?? "")

        case btn_Call:
            let link = "tel://\(self._AdsObj?._phone ?? "")"
            OpenSocialMedia(link: link)

        default:
            let activityViewController : UIActivityViewController = UIActivityViewController(
                activityItems: [(self._AdsObj?._title ?? ""),
                                "For more details check on our application available on App store & Play Store.",
                                "coming soon".localized]
                , applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = (sender)
            activityViewController.popoverPresentationController?.permittedArrowDirections = .any
            self.present(activityViewController, animated: true)

        }
    }
    
    func OpenSocialMedia(link: String) {
        if let _url = URL(string: link) {
            if UIApplication.shared.canOpenURL(_url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(_url, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(_url)
                }
            }
            else {
                print("error")
            }
        }
    }
    
    func sendEmail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setSubject("Send Mail")
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true, completion: nil)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func EditAction(_ sender : UIButton) {
        let vc : MEEditAdVC = AppDelegate.MainSB.instanceVC()
        vc._AdsObj = self._AdsObj
        vc.delegate = {
            self.navigationController?.popViewController(animated: true)
            self.getAdsDetails()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func DeleteAction(_ sender : UIButton) {
        self.showCustomAlert(okFlag: false, title: "", message: Auth_Strings.shared.DeleteAd(), okTitle: "Yes".localized, cancelTitle: "No".localized) { (action) in
            if action {
                self.DeleteAd()
            }
        }
    }
    
    func DeleteAd() {
        
        let url = Constant.AdsListURL + "/\(self._adsId)"
        let auth = Constant.HeaderToken
        
        self.showIndicator()
        MyApi.api.deleteRequestWihtoutValidate(url: url, auth: auth) { (error, response) in
            self.hideIndicator()

            if error != nil {
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                guard let res = response.dictionary else { return }
                print(res)
                self.delegate?()
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
    
    @IBAction func ShowMore(_ sender : UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.setImage("icon_ShowLess".toImage, for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.lbl_Content.numberOfLines = self.lbl_Content.calculateMaxLines()
                self.view.layoutIfNeeded()
            }
        }else{
            sender.setImage("icon_ShowMore".toImage, for: .normal)
            UIView.animate(withDuration: 0.4) {
                self.lbl_Content.numberOfLines = self._NumberOfLines
                self.view.layoutIfNeeded()
            }
        }
    }

}
