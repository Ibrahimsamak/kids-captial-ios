//
//  MEAdsListVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/7/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Firebase

class MEAdsListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btn_ScrollToTop: UIButton!

    var _AdsListArray : [AdsListST] = []
    var _cateId = ""
    var _offestY : CGFloat = 0
    
    private var _current = 1
    private var _total = 0
    private var isLoading = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "ADS".localized
        self.navigationItem.hideBackWord()
        
//        Analytics.setScreenName("Ads list page", screenClass: "Ads list page")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MEMyAdsCell")
        
        getAdsCategroy()
    }
    
    func getAdsCategroy() {
        
        let url = Constant.AdsListURL + "?ads_category_id=\(_cateId)" + "&page=\(_current)"
        let auth = Constant.HeaderAuth
        
        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                self._total = res["total"]?.int ?? 0
                let _dataArray = res["data"]?.array ?? []
            
                var tempArray : [AdsListST] = []
                for _data in _dataArray {
                    let obj = AdsListST(_Obj: _data)
                    tempArray.append(obj)
                }
                
                self._AdsListArray.append(contentsOf: tempArray)
                self.tableView.reloadData()
                self.isLoading = false
            }
        }
    }
    
    @IBAction func ScrollToTopAction(_ sender : UIButton) {
        self.tableView.scrollToTop()
    }
    
}

extension MEAdsListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _AdsListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _AdsListArray[indexPath.row]
        let vc : MEAdsDetailsVC = AppDelegate.MainSB.instanceVC()
        vc._adsId = "\(index._id)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MEAdsListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MEMyAdsCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let index = _AdsListArray[indexPath.row]
        
        cell.img_Ads.fetchImage(index._image)
        cell.lbl_Content.text = index._title
        cell.lbl_Date.text = index._created_at
        
        cell.img_Status.isHidden = true
        cell.lbl_Status.isHidden = true
        
        return cell
    }
}

extension MEAdsListVC {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        
        if scrollOffset <= 0 {
            self.btn_ScrollToTop.isHidden = true
            return
        }
        
        if (scrollOffset < _offestY) {
            self.btn_ScrollToTop.isHidden = false
            
        } else if (scrollOffset > _offestY) {
            self.btn_ScrollToTop.isHidden = true
            
        }
        
        if scrollOffset >= 0 {
            _offestY = scrollOffset
            
        } else if scrollOffset + scrollViewHeight == scrollContentSizeHeight {
            self.btn_ScrollToTop.isHidden = false
            
        }
        
        if ((scrollOffset + scrollViewHeight) >= (scrollContentSizeHeight))
        {
            if !isLoading {
                if (self._AdsListArray.count < self._total)
                {
                    isLoading = true
                    self._current += 1
                    self.getAdsCategroy()
                }
            }
        }
    }
}
