//
//  MENewAdVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/8/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit
import Photos
import TLPhotoPicker
import SwiftyPickerPopover
import IQKeyboardManagerSwift

class MENewAdVC: UIViewController  {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txt_Title: UITextField!
    @IBOutlet weak var txt_Phone: UITextField!
    @IBOutlet weak var txt_Category: UITextField!
    @IBOutlet weak var txt_Price: UITextField!
    @IBOutlet weak var txt_Desc: IQTextView!

    @IBOutlet var lbl_Collection: [UILabel]!
    
    var selectedAssets = [TLPHAsset]()
    var imageArray : [UIImage] = []
    var _AdsCateArray : [AdsCategoryST] = []
    var _Id = -1
    
    var delegate : (()->Void)?
    var _userInfo : UserST!
    var fromMyAds = false
    let timeStamp = Date().toMillis()!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "NEW AD".localized
        
        switchDirection()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MENewEditAdCell")
        collectionView.registerCell(id: "MEFirstCell")
        
        txt_Desc.placeholder = "Description".localized
        self._userInfo = UserST(Auth_User.ـUserInfo)
        
        if !DidOpenView._AdsCategoriesOpened {
            DidOpenView._AdsCategoriesOpened = true
            
            if Cached_Data._AdsCategories.isEmpty {
                self.showIndicator()
            }else{
                appendAdsCate()
            }
            
            getAdsCategroy()
            
        }else{
            appendAdsCate()
        }

    }
    
    func switchDirection() {
        for lbl in lbl_Collection {
            lbl.switchDirection()
        }
        
        txt_Title.switchDirection()
        txt_Phone.switchDirection()
        txt_Category.switchDirection()
        txt_Price.switchDirection()
        txt_Desc.switchDirection()
    }
    
    func getAdsCategroy() {
        
        let _time = _AdsCateArray.isEmpty ? 0 : _AdsCateArray.max {$0._timestamp < $1._timestamp}?._timestamp
        let url  = Constant.AdsCategoriesURL + "date=\(_time ?? 0)"
        let auth = Constant.HeaderAuth
        
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
                
                guard !_dataArray.isEmpty else {
                    return
                }

                let ud_tempArray  = _dataArray.map { return $0.dictionaryObject!}
                var _oldItems = Cached_Data._AdsCategories

                guard !ud_tempArray.isEmpty else {
                    return
                }

                if _oldItems.isEmpty {
                    Cached_Data._AdsCategories = ud_tempArray
                }else {
                    ud_tempArray.forEach({ (item) in
                        let _id = item["id"] as? Int ?? 0
                        let _changeType = item["changeType"] as? Int ?? 0
                        
                        let index = _oldItems.index(where: { (item) -> Bool in
                            (item["id"] as! Int) == _id
                        })
                        
                        if let _index = index {
                            switch _changeType {
                            case 2,1:
                                _oldItems[_index] = item
                            default:
                                _oldItems.remove(at: _index)
                            }
                        }else {
                            if _changeType == 1 || _changeType == 2 {
                                _oldItems.append(item)
                            }
                        }
                    })
                    Cached_Data._AdsCategories = _oldItems
                }
                
                self.appendAdsCate()
            }
        }
    }
    
    func appendAdsCate() {
        _AdsCateArray = Cached_Data._AdsCategories.map({ (item) -> AdsCategoryST in
            return AdsCategoryST(_Obj: item)
        })
        
        _AdsCateArray.forEach { (item) in
            if item._id == self._Id {
                self.txt_Category.text = item._name
            }
        }
        
        self.collectionView.reloadData()
    }
    
    @IBAction func PickCategory(_ sender : UIButton) {
        self.view.endEditing(true)
        let _name = _AdsCateArray.map {$0._name}
        StringPickerPopover(title: "Category".localized, choices: _name)
            .setSelectedRow(0)
            .setFont(UIFont.systemFont(ofSize: 18 , weight: .regular))
            .setDoneButton(title: "Done".localized,color: UIColor.white,action: { (popover, selectedRow, selectedString) in
                self._Id = self._AdsCateArray[selectedRow]._id
                self.txt_Category.text = selectedString
            })
            .setCancelButton(title:"Cancel".localized, color: UIColor.white, action: { (_, _, _) in print("cancel")}
            )
            .appear(originView: sender, baseViewController: self)
    }

    @IBAction func SendAd(_ sender : UIButton) {
        
        guard let _Title = txt_Title.text?.TrimWhiteSpaces, !_Title.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.TitleRequired())
            return
        }
        
        guard let Phone = txt_Phone.text?.TrimWhiteSpaces, !Phone.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.PhoneRequired())
            return
        }

        if self._Id == -1 {
            self.showOkAlert(title: "", message: Auth_Strings.shared.CategoryRequired())
            return
        }
        
//        guard let Price = txt_Price.text?.TrimWhiteSpaces, !Price.isEmpty else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.PriceRequired())
//            return
//        }

        guard let Desc = txt_Desc.text?.TrimWhiteSpaces, !Desc.isEmpty else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.DescRequired())
            return
        }
        
        let url  = Constant.AdsListURL
        let auth = Constant.HeaderTokenLang
        
        let param = ["title" : _Title,
                     "details" : Desc,
                     "price" : txt_Price.text?.TrimWhiteSpaces ?? "",
                     "ads_category_id" : _Id,
                     "email" : _userInfo._email,
                     "mobile" : Phone] as [String:Any]

        self.view.endEditing(true)
        self.showIndicator()
        MyApi.api.postRequestWithoutValidate(url: url, para: param, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{

                guard let res = response.dictionary else { return }
                let _data = res["data"]?.dictionary ?? [:]
                let errors = res["errors"]?.dictionaryObject ?? [:]

                print(res)
                if errors.isEmpty {
                    if !_data.isEmpty {
                        let _id = _data["id"]?.int
                        if self.imageArray.isEmpty {
                            self.hideIndicator()
                            self.showOkAlertWithComp(title: "", message: "Successfully Added", completion: { (_) in
                                self.navigationController?.popViewController(animated: true)
                                self.delegate?()
                            })
                        }else{
                            self.UploadImages(_id: _id!)
                        }
                    }
                }else{
                    self.hideIndicator()
                    self.showOkAlert(title: "", message: errors.printErrors())
                }
            }
        }

    }
    
    func UploadImages(_id: Int) {
        
        let url = Constant.AdsListURL + "/\(_id)" + "/addImage"
        let auth = Constant.HeaderToken
        
        var count = 0
        for img in imageArray {
            MyApi.api.upload_image_multipart(url: url, header: auth, p1_image: img) { (error, response) in
                if error != nil {
                    self.hideIndicator()
                    self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                    return
                }else{
                    count += 1
                    guard let res = response.dictionary else { return }
                    let errors = res["errors"]?.dictionaryObject ?? [:]

                    if (self.imageArray.count) == count {
                        self.hideIndicator()
                        self.showOkAlertWithComp(title: "", message: "Successfully Added", completion: { (_) in
                            self.navigationController?.popViewController(animated: true)
                            self.delegate?()
                        })
                    }
                    
                }
            }
        }
    }
}

extension MENewAdVC : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (imageArray.count) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        if index == 0 {
            self.PickPhotos()
        }
    }
    
    func PickPhotos() {
        let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        
        var configure = TLPhotosPickerConfigure()
        configure.allowedVideo = false
        configure.allowedLivePhotos = false
        configure.allowedVideoRecording = false
        configure.mediaType = .image
        
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.configure = configure
        
        UIApplication.shared.statusBarView?.backgroundColor = .black
        self.present(viewController, animated: true, completion: nil)
    }
    

}

extension MENewAdVC : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            let cell: MEFirstCell = collectionView.dequeueCVCell(indexPath: indexPath)
            
            return cell
        }else{
            let cell : MENewEditAdCell = collectionView.dequeueCVCell(indexPath: indexPath)
            
            cell.img_Image.image = self.imageArray[indexPath.row - 1]
            
            return cell
        }
    }
}

extension MENewAdVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 100)
    }
}

extension MENewAdVC : TLPhotosPickerViewControllerDelegate {
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset]) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear

        self.imageArray = []
        for img in withTLPHAssets {
            self.imageArray.append(getUIImage(asset: img.phAsset!))
        }
        
        self.selectedAssets = withTLPHAssets
        self.collectionView.reloadData()
    }
    
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    func photoPickerDidCancel() {
        UIApplication.shared.statusBarView?.backgroundColor = .clear
    }
    
    func dismissComplete() { }
    
    func canSelectAsset(phAsset: PHAsset) -> Bool { return true }
    
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) { }
    
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) { }
    
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) { }
    
    func getUIImage(asset: PHAsset) -> UIImage {
        
        var img = UIImage()
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data) ?? UIImage()
            }
        }
        return img
    }
    
}
