//
//  MEPreviewImageVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEPreviewImageVC: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var btn_Close: UIButton!
    
    var _Image = ""
    var _isAd = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
        self.img_Image.fetchImage(_Image)
        
        let btn_Image = _isAd ? "" : ""
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return img_Image
    }
    
    @IBAction func DismissView(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
