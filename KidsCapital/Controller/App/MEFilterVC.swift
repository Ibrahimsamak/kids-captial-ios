//
//  MEFilterVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/4/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEFilterVC: UIViewController {

    @IBOutlet weak var btn_Area: UIButton!
    @IBOutlet weak var btn_Category: UIButton!
    @IBOutlet weak var btn_Facilities: UIButton!

    @IBOutlet weak var view_Coupon: UIView!

    @IBOutlet weak var lbl_Area: UILabel!
    @IBOutlet weak var lbl_Category: UILabel!
    @IBOutlet weak var lbl_Facility: UILabel!

    var _selectedIndex = -1
    var delegate : ((String,String)->Void)?
    
    var _AreaName = ""
    var _CategoryName = ""
    var _FacilityName = ""
    
    var _AreaId = -1
    var _CategoryId = -1
    var _FacilityId = -1

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "FILTER".localized
        self.navigationItem.hideBackWord()
    }
    
    
    @IBAction func MultiAction(_ sender : UIButton) {
        let vc : MEFilterResultVC = AppDelegate.MainSB.instanceVC()
        vc.delegate = { (_FilterType, _Id, _Name) in
            switch _FilterType {
            case .Area:
                
                self.lbl_Area.text = _Name
                self._AreaId = _Id
                self._AreaName = _Name

            case .Category:
                
                self.lbl_Category.text = _Name
                self._CategoryId = _Id
                self._CategoryName = _Name
                
            default:
                
                self.lbl_Facility.text = _Name
                self._FacilityId = _Id
                self._FacilityName = _Name
                
            }
        }
        
        switch sender {
        case btn_Area:
            
            vc._Type = .Area
            vc._selectedName = _AreaName
            vc._selectedIndex = _AreaId
            
        case btn_Category:
            
            vc._Type = .Category
            vc._selectedName = _CategoryName
            vc._selectedIndex = _CategoryId
            
        default:
            
            vc._Type = .Facility
            vc._selectedName = _FacilityName
            vc._selectedIndex = _FacilityId
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func ApplyAction(_ sender : UIButton) {
        
        guard _AreaName != "" || _CategoryName != "" || _FacilityName != "" else {
            self.showOkAlert(title: "", message: Auth_Strings.shared.ChooseFilterType())
            return
        }
        
        let _titleArray = [_AreaName,_CategoryName,_FacilityName]
        let _IdsArray = [_AreaId,_CategoryId,_FacilityId]
        
        var filteredTitle : [String] = []
        var filteredIds : [String] = []
        
        for (index,_title) in _titleArray.enumerated() {
            if _title != "" {
                filteredTitle.append(_title)
                filteredIds.append("\(_IdsArray[index])")
            }
        }
        
        let finalTitle = filteredTitle.joined(separator: ",")
        let finalIds = filteredIds.joined(separator: ",")
        
        let finalName = finalTitle + " " + finalIds
        
        var url = "&area_id=\(_AreaId)&category_id=\(_CategoryId)&facility_id=\(_FacilityId)"
        url = url.replacingOccurrences(of: "-1", with: "")
        
        self.delegate?(url,finalName)
        self.navigationController?.popViewController(animated: true)
        
        //        guard _AreaId != -1 else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.AreaRequired())
//            return
//        }
//
//        guard _CategoryId != -1 else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.CategoryRequired())
//            return
//        }
//
//        guard _FacilityId != -1 else {
//            self.showOkAlert(title: "", message: Auth_Strings.shared.FacilityRequired())
//            return
//        }

        
    }
    
    @IBAction func ClearTypes(_ sender : UIButton) {
        self._AreaName = ""
        self._CategoryName = ""
        self._FacilityName = ""
        self._AreaId = -1
        self._CategoryId = -1
        self._FacilityId = -1
        
        self.delegate?("", "")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
