//
//  MEGalleryImagesVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEGalleryImagesVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var view_Coupon: UIView!

    var _ImagesArray : [String] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _newCoupon {
            view_Coupon.isHidden = Auth_User._Token != "" && Auth_User._UserType != 2 ? true : false
        }else{
            view_Coupon.isHidden = true
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "GALLERY".localized
        self.navigationItem.hideBackWord()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: "MEGalleryImagesCell")
    }

    @IBAction func ShowCoupons(_ sender : UIButton) {
        if Auth_User._Token == "" {
            self.showOkAlert(title: "", message: Auth_Strings.shared.LoginRequired())
            return
        }
        let vc : MECouponsVC = AppDelegate.MainSB.instanceVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension MEGalleryImagesVC : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _ImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / 2) - 34
        return CGSize(width: width , height: width + 50/*204*/)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc : MEPreviewImageVC = AppDelegate.MainSB.instanceVC()
        vc._Image = _ImagesArray[indexPath.item]
        self.present(vc, animated: true)
    }
}

extension MEGalleryImagesVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : MEGalleryImagesCell = collectionView.dequeueCVCell(indexPath: indexPath)
        
        cell.img_Image.fetchImage(_ImagesArray[indexPath.item])
    
        return cell
    }
}
