//
//  MECoupansVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MECouponsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var _couponArray : [CouponST] = []
    var _myCoupon = false

    private var isLoading = true
    private var _current = 1
    private var _total = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = _myCoupon ? "MY COUPONS".localized : "COUPONS".localized
        self.navigationItem.hideBackWord()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MECouponsCell")
        

        getCoupons()
    }
    
    func getCoupons() {

        var url = _myCoupon ? Constant.MyCouponsURL : Constant.CouponsURL
        url += "?page=\(_current)"
        
        let auth = Constant.HeaderTokenLang

        self.showIndicator()
        MyApi.api.gettReqWithHeader(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{

                self.hideIndicator()
                guard let res = response.dictionary else { return }
                self._total = res["total"]?.int ?? 0
                let _dataArray = res["data"]?.array ?? []

                _newCoupon = _dataArray.isEmpty ? false : true
                var tempArray : [CouponST] = []
                
                for _data in _dataArray {
                    let obj = CouponST(_data)
                    tempArray.append(obj)
                }
                
                self._couponArray.append(contentsOf: tempArray)

                self.tableView.reloadData()
                self.isLoading = false
            }
        }
    }
    
}

extension MECouponsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _couponArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _couponArray[indexPath.row]
        let vc : MECoupansDeatilsVC = AppDelegate.MainSB.instanceVC()
        vc._obj = index
        vc._myCoupon = self._myCoupon
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MECouponsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MECouponsCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let index = _couponArray[indexPath.row]
        
        cell.lbl_Title.text = index._title
        cell.lbl_Date.text = index._start_date
        
        return cell
    }
}

extension MECouponsVC {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y

        if ((scrollOffset + scrollViewHeight) >= (scrollContentSizeHeight))
        {
            if !isLoading {
                if (self._couponArray.count < self._total)
                {
                    isLoading = true
                    self._current += 1
                    self.getCoupons()
                }
            }
        }
    }
}
