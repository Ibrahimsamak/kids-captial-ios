//
//  MEMyAdsVC.swift
//  KidsCapital
//
//  Created by iosMaher on 12/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import UIKit

class MEMyAdsVC: UIViewController {

    @IBOutlet weak var layout_TopLabel: NSLayoutConstraint!
    @IBOutlet weak var layout_TopButton: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_NoAds: UIView!
    
    var _AdsListArray : [AdsListST] = []
    var _isMyAds = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "MY ADS".localized
        self.navigationItem.hideBackWord()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "MEMyAdsCell")
        
        switch UIScreen.main.sizeType {
        case .iPhone5:
            layout_TopLabel.constant = 5
            layout_TopButton.constant = 5
        default:
            break
        }
        
        getMyAds()
    }
    
    func getMyAds() {
        
        _AdsListArray = []
        
        let url = Constant.MyAdsListURL
        let auth = Constant.HeaderToken
        
        self.showIndicator()
        MyApi.api.gettReqWithHeaderURLEnc(url: url, auth: auth) { (error, response) in
            if error != nil {
                self.hideIndicator()
                self.showOkAlert(title: "", message: error?.localizedDescription ?? "")
                return
            }else{
                
                self.hideIndicator()
                guard let res = response.dictionary else { return }
                let _dataArray = res["data"]?.array ?? []
//                let message = res["message"]?.string ?? ""

                for _data in _dataArray {
                    let obj = AdsListST(_Obj: _data)
                    self._AdsListArray.append(obj)
                }
                
                if self._AdsListArray.isEmpty {
                    self.view_NoAds.isHidden = false
                }else{
                    self.view_NoAds.isHidden = true
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func PostAdAction(_ sender : UIButton) {
        let vc : MENewAdVC = AppDelegate.MainSB.instanceVC()
        vc.delegate = {
            self.getMyAds()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MEMyAdsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _AdsListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = _AdsListArray[indexPath.row]
        let vc : MEAdsDetailsVC = AppDelegate.MainSB.instanceVC()
        
        vc.delegate = {
            self.getMyAds()
        }
        
        vc._adsId = "\(index._id)"
        vc._isMyAds = self._isMyAds
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MEMyAdsVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MEMyAdsCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let index = _AdsListArray[indexPath.row]
        cell.lbl_Content.text = index._title
        cell.lbl_Date.text = index._created_at
        
        cell.img_Ads.fetchImage(index._image)
        cell.img_Status.fetchImage(index._status_icon)
        cell.lbl_Status.text = index._status_value
        
        return cell
    }
}
