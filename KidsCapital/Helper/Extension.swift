//
//  Extension.swift
//  Chalk Store
//
//  Created by iosMaher on 7/2/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation
import UIKit
//import SystemConfiguration
import AVFoundation
import PopupDialog
import SwiftyJSON
import SKActivityIndicatorView
import SDWebImage

extension Dictionary {
    
    
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
    
    func printErrors() -> String {
        var str = ""
        for key in self.keys {
            let val = (self[key] as? [String] ?? []).map{$0}.joined(separator: "\n")
            str += (val + "\n")
        }
        str = String(str.dropLast(2))
        return str
    }
}

extension UIColor {
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor
    {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }

    //random color
    static func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    func colorToImage() -> UIImage
    {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}

extension UINavigationBar {
    
    func setNavigationClear() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
    
    func removeNavigationClear() {
//        self.setBackgroundImage("131825".color.colorToImage(), for: .default)
//        self.shadowImage = "131825".color.colorToImage()
        self.isTranslucent = false
    }

    var shadow: Bool {
        get {
            return false
        }
        set {
            if newValue {
                self.layer.shadowOffset = CGSize(width: 0, height: 2)
                self.layer.shadowColor = UIColor.lightGray.cgColor
                self.layer.shadowRadius = 3
                self.layer.shadowOpacity = 0.5;
            }
        }
    }
}

extension UIView
{
    
    func setRounded() {
        let radius = self.frame.height / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRounded(radius:CGFloat) {
        self.layer.cornerRadius = radius
//        self.layer.masksToBounds = true
    }

    func round(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func setBorder(width:CGFloat,color:CGColor) {
        self.layer.borderColor = color
        self.layer.borderWidth = width
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addShadowView(width:Int = 0 ,height:Int = 0,opacity:Float = 1,radius:CGFloat = 10,color:UIColor = UIColor.black.withAlphaComponent(0.1),cornerRadius : CGFloat = 4) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = false
    }
    
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
    }
    
    func toPngImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let pngImageData: Data? = image?.pngData()
        let finalImage = UIImage(data: pngImageData!)
        return finalImage!
    }
}

extension UITableView {
    
    func registerCell(id: String) {
        self.register(UINib(nibName: id, bundle: nil), forCellReuseIdentifier: id)
    }
    
    func setAndLayoutTableHeaderView(header: UIView) {
        self.tableHeaderView = header
        header.setNeedsLayout()
        header.layoutIfNeeded()
        header.frame.size = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        self.tableHeaderView = header
    }
}

extension UIViewController {
    
    func showIndicator(message : String = "") {
        SKActivityIndicator.statusLabelFont(UIFont.systemFont(ofSize: 14))
        SKActivityIndicator.spinnerStyle(.spinningCircle)
        SKActivityIndicator.spinnerColor(.black)
        SKActivityIndicator.show(message, userInteractionStatus: false)
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func errorArray(_ ErrorArray : [String : JSON]) -> String {
        
        let error = ErrorArray["errors"]?.array
        var errorMessages = ""
        for err in error! {
            errorMessages = errorMessages + "\(err)" + "\n"
        }
        
        return errorMessages
    }
    
    func RootViewController(viewController:UIViewController) {
        guard let window = UIApplication.shared.keyWindow else { return }
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: nil, completion: nil)
    }
    
//    func isConnected() {
//        guard self.isConnectedToNetwork() else {
//            return
//        }
//    }
//
//    func isConnectedToNetwork() -> Bool {
//
//        var zeroAddress = sockaddr_in()
//        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//
//        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
//            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
//                SCNetworkReachabilityCreateWithAddress(nil, $0)
//            }
//        }) else {
//            return false
//        }
//
//        var flags: SCNetworkReachabilityFlags = []
//        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
//            return false
//        }
//
//        let isReachable = flags.contains(.reachable)
//        let needsConnection = flags.contains(.connectionRequired)
//
//        return (isReachable && !needsConnection)
//    }
    
    func showOkAlert(title:String,message:String) {
        
        // Create the dialog
        let popup = PopupDialog.init(title: title,
                                     message: message,
                                     image: UIImage(),
                                     buttonAlignment: .horizontal,
                                     transitionStyle: .zoomIn,
                                     preferredWidth: self.view.frame.width,
                                     tapGestureDismissal: false,
                                     panGestureDismissal: false,
                                     hideStatusBar: true, completion: nil)

        let okButton = DefaultButton(title: "Ok".localized) {
            popup.dismiss()
        }
        
        popup.addButton(okButton)
        self.present(popup, animated: true, completion: nil)
    }
    
    func showCustomAlert(okFlag:Bool,title:String,message:String,okTitle:String,cancelTitle:String,completion:@escaping (Bool) -> Void) {
        
        // Create the dialog
        let popup = PopupDialog.init(title: title,
                                     message: message,
                                     image: UIImage(),
                                     buttonAlignment: .horizontal,
                                     transitionStyle: .zoomIn,
                                     preferredWidth: self.view.frame.width,
                                     tapGestureDismissal: false,
                                     panGestureDismissal: false,
                                     hideStatusBar: true, completion: nil)

        let cancelButton = CancelButton(title: cancelTitle) {
            completion(false)
            popup.dismiss()
        }
        
        cancelButton.titleColor = UIColor.red
        
        let okButton = DefaultButton(title: okTitle) {
            completion(true)
        }
        
        if okFlag {
            popup.addButton(okButton)
        }else{
            popup.addButtons([cancelButton,okButton])
        }
        
        self.present(popup, animated: true, completion: nil)
    }
    
    func showOkAlertWithComp(title:String,message:String,completion:@escaping (Bool) -> Void) {
        
        // Create the dialog
        let popup = PopupDialog.init(title: title,
                                     message: message,
                                     image: UIImage(),
                                     buttonAlignment: .horizontal,
                                     transitionStyle: .zoomIn,
                                     preferredWidth: self.view.frame.width,
                                     tapGestureDismissal: false,
                                     panGestureDismissal: false,
                                     hideStatusBar: true, completion: nil)

        let okButton = DefaultButton(title: "Ok".localized) {
            completion(true)
        }
        
        popup.addButton(okButton)
        self.present(popup, animated: true, completion: nil)
    }
    
}

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

extension UIStoryboard {
    func instanceVC<T: UIViewController>() -> T {
        guard let vc = instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return vc
    }
}

extension UICollectionView {
    func registerCell(id: String) {
        self.register(UINib(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
    }
}

extension UITableView {
    func dequeueTVCell<T: UITableViewCell>() -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return cell
    }
}
extension UICollectionView {
    func dequeueCVCell<T: UICollectionViewCell>(indexPath:IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return cell
    }
}

extension String
{
    
    func convertStringFormat(defaultFormat: String, toFormat: String) -> String {
        
        let toDate = DateFormatter()
        toDate.dateFormat = defaultFormat
        let newDate = toDate.date(from: self)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = toFormat
        let dateToStr = dateFormatter.string(from: newDate ?? Date())
        
        return dateToStr
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : font], context: nil)
        
        return boundingBox.height
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    //cut first caracters from full names
    public func getAcronyms(separator: String = "") -> String {
        let acronyms = self.components(separatedBy: " ").map({ String($0.first!) }).joined(separator: separator);
        return acronyms;
    }
    
    //remove spaces from text
    var trimmed:String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var isEmailValid: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    var isEmptyStr:Bool{
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces).isEmpty
    }
    
    var color: UIColor {
        let hex = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            return UIColor.clear
        }
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    var cgColor: CGColor {
        return self.color.cgColor
    }
    
    var TrimAllSpaces:String {
        return self.components(separatedBy: .whitespacesAndNewlines).joined()
    }
    
    var TrimWhiteSpaces:String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var removeHtmlTags:String {
        //        return   self.replacingOccurrences(of: "<div*></div>", with: "")
        let regex:NSRegularExpression  = try! NSRegularExpression(  pattern: "<.*?>", options: .caseInsensitive)
        let range = NSMakeRange(0, self.count)
        let htmlLessString :String = regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(), range:range , withTemplate: "")
        return htmlLessString.replacingOccurrences(of: "&amp;", with: "")
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var toImage: UIImage {
        if self == "" {
            return UIImage(named: "img_Placeholder") ?? UIImage()
        }else{
            return UIImage(named: self)!.imageFlippedForRightToLeftLayoutDirection()
        }
    }
    
    var containsWhitespace : Bool {
        return (self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    var containtSpecialCharacter : Bool {
        let characterset = CharacterSet(charactersIn: "+:!@#$%^&*()=<>?/\\';~`٫,,،±§-|€}{[]\"∙•¥£")
        
        return self.rangeOfCharacter(from: characterset) != nil ?  true : false
    }
    
    func replacingCharacter(newStr:String,range:NSRange) -> String {
        let str = NSString(string: self).replacingCharacters(in: range, with : newStr) as NSString
        return String(str)
    }
}

extension UIImageView {
    func fetchImage(_ imgURL : String) {
        if let imgURL = URL.init(string: imgURL) {
            self.sd_setIndicatorStyle(.gray)
            self.sd_setShowActivityIndicatorView(true)
            self.sd_setImage(with: imgURL, placeholderImage: "img_Placeholder".toImage)
        }else{
            self.image = "img_Placeholder".toImage
        }
    }
}

extension UITextView {
    func switchDirection() {
        if Auth_User._Language == "ar" {
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    }
}

extension UITextField
{
    
    func switchDirection() {
        if Auth_User._Language == "ar" {
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    }
}

extension UINavigationController
{
    func pop(animated: Bool) {
        _ = self.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
}

extension UINavigationItem
{
    func hideBackWord()  {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.backBarButtonItem = backItem
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}

extension DateFormatter {
    
    func timeSince(from: Date, numericDates: Bool = false) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = now.earlierDate(from as Date)
        let latest = earliest == now as Date ? from : now as Date
        let components = calendar.dateComponents([.year, .weekOfYear, .month, .day, .hour, .minute, .second], from: earliest, to: latest as Date)
        
        var result = ""
        
        if components.year! >= 2 {
            result = "\(components.year!) years ago"
            
        } else if components.year! >= 1 {
            result = "year ago"
            
        } else if components.month! >= 2 {
            result = "\(components.month!) monthes ago"
            
        } else if components.month! >= 1 {
            result = "month ago"
            
        } else if components.weekOfYear! >= 2 {
            result = "\(components.weekOfYear!) weeks"
            
        } else if components.weekOfYear! >= 1 {
            result = "week ago"
            
        } else if components.day! >= 2 {
            result = "\(components.day!) days"
            
        } else if components.day! >= 1 {
            result = "day ago"
            
        } else if components.hour! >= 2 {
            result = "\(components.hour!) hrs"
            
        } else if components.hour! >= 1 {
            result = "1 hr"
            
        } else if components.minute! >= 2 {
            result = "\(components.minute!) mins"
            
        } else if components.minute! >= 1 {
            result = "1 min"
            
        } else if components.second! >= 3 {
            result = "\(components.second!) sec"
        } else {
            result = "now"
        }
        
        return result
    }
}

// Set Gradient To Any View

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        get { return points.startPoint }
    }
    
    var endPoint : CGPoint {
        get { return points.endPoint }
    }
    
    var points : GradientPoints {
        get {
            switch(self) {
            case .topRightBottomLeft:
                return (CGPoint.init(x: 0.0,y: 1.0), CGPoint.init(x: 1.0,y: 0.0))
            case .topLeftBottomRight:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 1,y: 1))
            case .horizontal:
                return (CGPoint.init(x: 0.0,y: 0.5), CGPoint.init(x: 1.0,y: 0.5))
            case .vertical:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 0.0,y: 1.0))
            }
        }
    }
}

extension Double {
    
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    mutating func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
    
}

extension UILabel
{
    
    func switchDirection() {
        if Auth_User._Language == "ar" {
            self.textAlignment = .right
        }else{
            self.textAlignment = .left
        }
    }

    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }

    func setUnderLine() {
        let textRange = NSMakeRange(0, self.text!.count)
        let attributedText = NSMutableAttributedString(string: self.text!)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle,
                                    value:NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        
        self.attributedText = attributedText
    }
    
    func setStrikethroughStyle(text:String) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
    
}

extension UIScreen {
    
    enum SizeType: CGFloat {
        case iPhone5 = 1136.0
        case iPhone6_7_8 = 1334.0
        case iPhone6_7_8Plus = 1920.0
        case iPhoneXR = 1624
        case iPhoneX_XS = 2436
        case iPhoneXsMax = 2688
        case Unknown = 2048
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
}
