//
//  Auth_user.swift
//  dyaftcom
//
//  Created by MyTools.swift on 2/27/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation

struct Auth_User {

    static var _firstOpen: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "firstOpen") as? Bool ?? true
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "firstOpen")
            ud.synchronize()
        }
    }
    
    static var _CateTimeStamp : Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "cateTimeStamp") as? Int ?? 0
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "cateTimeStamp")
            ud.synchronize()
        }
    }

    static var _Token : String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "access_token") as? String ?? ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "access_token")
            ud.synchronize()
        }
    }

    static var _UserType : Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "userType") as? Int ?? -1
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "userType")
            ud.synchronize()
        }
    }
    
    static var _CategoryId : Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "categoryId") as? Int ?? -1
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "categoryId")
            ud.synchronize()
        }
    }

    static var _ProvinceId : Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "provinceId") as? Int ?? -1
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "provinceId")
            ud.synchronize()
        }
    }

    static var _CategoryName : String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "categoryName") as? String ?? ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "categoryName")
            ud.synchronize()
        }
    }

    static var _provinceName : String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "provinceName") as? String ?? ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "provinceName")
            ud.synchronize()
        }
    }

    static var ـUserInfo: [String:Any] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "User_Data") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String:Any] {
                return userData
            }
            return [:]
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "User_Data")
        }
    }

    static var _Language : String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "lang") as? String ?? "en"
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "lang")
            ud.synchronize()
        }
    }

    static func removeUserInfo() {
        let ud = UserDefaults.standard
        ud.removeObject(forKey: "User_Data")
        ud.removeObject(forKey: "access_token")
        ud.synchronize()
    }

}

struct Cached_Data {
    
    static var _SponsoredArray : [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "SponsoredArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "SponsoredArray")
        }
    }
    
    static var _ProvienceArray: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "ProvienceArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "ProvienceArray")
        }
    }
    
//    static var _ProvienceArray_Ar: [[String:Any]] {
//        get {
//            let ud = UserDefaults.standard
//            let data = ud.value(forKey: "ProvienceArray_Ar") as? Data ?? Data()
//            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
//                return userData
//            }
//            return []
//        }
//        set(userinfo) {
//            let ud = UserDefaults.standard
//            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
//            ud.set(encodedData, forKey: "ProvienceArray_Ar")
//        }
//    }

    
    static var _AreaArray: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "AreaArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "AreaArray")
        }
    }
    
    static var _CategoryArray: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "CategoryArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "CategoryArray")
        }
    }
    
    static var _FacilitesArray: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "FacilitesArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "FacilitesArray")
        }
    }
    
    static var _AdsCategories: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "AdsArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "AdsArray")
        }
    }

    static var _PagesArray: [[String:Any]] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "PagesArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [[String:Any]] {
                return userData
            }
            return []
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "PagesArray")
        }
    }

    static var _ContactUsObject: [String:Any] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "ContactusArray") as? Data ?? Data()
            if let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String:Any] {
                return userData
            }
            return [:]
        }
        set(userinfo) {
            let ud = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: userinfo)
            ud.set(encodedData, forKey: "ContactusArray")
        }
    }

}

struct DidOpenView {
    
    static var _ProvinceOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "ProvinceOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "ProvinceOpened")
            ud.synchronize()
        }
    }

    static var _CategoryOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "CategoryOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "CategoryOpened")
            ud.synchronize()
        }
    }

    static var _AreaOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "AreaOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "AreaOpened")
            ud.synchronize()
        }
    }

    static var _FacilitesOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "FacilitesOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "FacilitesOpened")
            ud.synchronize()
        }
    }
    
    static var _AdsCategoriesOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "AdsCategoriesOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "AdsCategoriesOpened")
            ud.synchronize()
        }
    }

    static var _PagesOpened: Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "PagesOpened") as? Bool ?? false
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "PagesOpened")
            ud.synchronize()
        }
    }

    
}
