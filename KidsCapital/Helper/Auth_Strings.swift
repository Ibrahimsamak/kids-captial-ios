//
//  Auth_String.swift
//  Chalk Store
//
//  Created by iosMaher on 7/5/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation

class Auth_Strings {
    
    static var shared = Auth_Strings()

    func ChooseProvience() -> String {
        return "Please select province".localized
    }

    func ChooseCategory() -> String {
        return "Please select category".localized
    }

    func AreaFilter() -> String {
        return "Please select area".localized
    }

    func AreaFacility() -> String {
        return "Please select facility".localized
    }
    
    func ResetCode() -> String {
        return "The code field is required".localized
    }

    func CouponCode() -> String {
        return "The coupon code field is required".localized
    }

    func NurseryId() -> String {
        return "The nursery id field is required".localized
    }

    func DescRequired() -> String {
        return "The description field is required".localized
    }

    func PriceRequired() -> String {
        return "The price field is required".localized
    }

    func TitleRequired() -> String {
        return "The title field is required".localized
    }

    func Logout() -> String {
        return "Are you sure you want to logout?".localized
    }

    func DeleteAd() -> String {
        return "Are you sure you want to delete the ad?".localized
    }
    
    func PhoneRequired() -> String {
        return "The phone field is required".localized
    }

    func FullnameRequired() -> String {
        return "The fullname field is required".localized
    }

    func UsernameRequired() -> String {
        return "The username field is required".localized
    }

    func LoginRequired() -> String {
        return "You must sign in".localized
    }
    
    func AcceptTerms() -> String {
        return "Please accept terms".localized
    }

    func PasswordRequired() -> String {
        return "The password field is required".localized
    }

    func NewPasswordRequired() -> String {
        return "The new password field is required".localized
    }

    func ConfirmPasswordRequired() -> String {
        return "The confirm password field is required".localized
    }
    
    func OldPasswordRequired() -> String {
        return "The old password field is required".localized
    }

    func PasswordsNotMatch() -> String {
        return "The confirmation password and password must match".localized
    }

    func InvalidEmail() -> String {
        return "Please enter valid email".localized
    }

    func EmailRequired() -> String {
        return "The email field is required".localized
    }

    func UserEmailRequired() -> String {
        return "Username/email field is required".localized
    }

    func ChooseFilterType() -> String {
        return "Please choose filter type".localized
    }

    func ChooseSearchType() -> String {
        return "Please choose search type".localized
    }

    func NoWebsite() -> String {
        return "There is no website".localized
    }
    
    func NoEmail() -> String {
        return "There is no email avaliable".localized
    }

    func AreaRequired() -> String {
        return "Please choose area".localized
    }

    func CategoryRequired() -> String {
        return "Please choose category".localized
    }
    
    func ProvienceRequired() -> String {
        return "Please choose provience".localized
    }
    
    func FacilityRequired() -> String {
        return "Please choose facility".localized
    }

    func SocialMediaNotExist() -> String {
        return "There is no link".localized
    }

}
