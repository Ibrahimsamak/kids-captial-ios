//
//  Helper.swift
//  Chalk Store
//
//  Created by iosMaher on 7/8/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation

import UIKit

class Helper  {
    
    static var shared = Helper()
    
    
    func func_openUrl(mainUrl:String,scondaryUrl:String , schema:String) {
        
        let strUrl: String = mainUrl
        
        if UIApplication.shared.canOpenURL(URL(string: schema)!) {
            let url = URL(string: strUrl)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            
            if let url = URL(string: scondaryUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

}
