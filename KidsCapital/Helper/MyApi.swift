
import Foundation
import Alamofire
import SwiftyJSON

class MyApi: NSObject {
    
    static var api = MyApi()
    
    func upload_image_multipart(url: String ,header: [String:String], p1_image: UIImage ,completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        
        let p1 = p1_image.jpegData(compressionQuality: 0.5)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(p1!, withName: "images[]", fileName: "image\(arc4random_uniform(100)).jpeg", mimeType: "image/jpeg")
            
        }, to:url, method: .post,headers: header)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print(Progress)
                })
                
                upload.responseJSON { response in
                    
                    switch response.result {
                    case .failure(let error):
                        let jsonError = JSON(error)
                        completion(error,jsonError)
                    case .success(let value):
                        let json = JSON(value)
                        completion(nil,json)
                    }
                }
            case .failure(let encodingError):
                let jsonError = JSON(encodingError)
                completion(encodingError,jsonError)
            }
        }
    }

    
    func upload_image_multipart_with_param(url: String,Parameters : [String:Any] ,header: [String:String], p1_image: UIImage ,completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        
        let p1 = p1_image.jpegData(compressionQuality: 0.5)
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(p1!, withName: "img", fileName: "image\(arc4random_uniform(100)).jpeg", mimeType: "image/jpeg")

            
            for (key, value) in Parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

        }, to:url, method: .post,headers: header)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print(Progress)
                })
                
                upload.responseJSON { response in
                    
                    switch response.result {
                    case .failure(let error):
                        let jsonError = JSON(error)
                        completion(error,jsonError)
                    case .success(let value):
                        let json = JSON(value)
                        completion(nil,json)
                    }
                }
            case .failure(let encodingError):
                let jsonError = JSON(encodingError)
                completion(encodingError,jsonError)
            }
        }
    }
    
    func sendRequestPostJsonParam(urlString:String , param:[String:Any] , completion:((NSDictionary?,String,Bool)->Void)!)
    {
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: param)
        
        Alamofire.request(request)
            .responseJSON { response in
                // do whatever you want here
                if(response.result.isSuccess)
                {
                    if  let resp = response.result.value as? NSDictionary {
                        let Status = resp["Status"] as? Bool ?? false
                        let StatusResultText = resp["StatusResultText"] as? String ?? ""
                        completion(resp,StatusResultText,Status)
                    }else {
                        completion(nil,"ServerError".localized,false)
                    }
                }
                else
                {
                    completion(nil,response.result.error.debugDescription,false)
                }
        }
    }
    
    func postReqWithHeader(url:String,para: [String:Any], auth : [String:String],completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        Alamofire.request(url, method: .post, parameters: para, encoding: JSONEncoding.default, headers: auth)
            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }
    
    func gettReqWithHeader(url:String,
                           auth : [String:String],
                           completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
//        Alamofire.SessionManager.default.requestWithoutCache(url)
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: auth)
//            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }
    
    
    func gettReqWithHeaderURLEnc(url:String,
                           auth : [String:String],
                           completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: auth)
            //            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }
    
    func postRequestWithoutValidate(url:String,para: [String:Any], auth : [String:String],completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        Alamofire.request(url, method: .post, parameters: para, encoding: URLEncoding.default, headers: auth)
//            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }

    func putRequestWihtoutValidate(url:String,para: [String:Any], auth : [String:String],completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        Alamofire.request(url, method: .put, parameters: para, encoding: URLEncoding.default, headers: auth)
            //            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }

    func deleteRequestWihtoutValidate(url:String, auth : [String:String],completion: @escaping (_ error:Error?,_ response:JSON) -> Void) {
        
        Alamofire.request(url, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: auth)
            //            .validate(statusCode: 200..<300)
            .responseJSON { response  in
                
                switch response.result {
                case .failure(let error):
                    let jsonError = JSON(error)
                    completion(error,jsonError)
                case .success(let value):
                    let json = JSON(value)
                    completion(nil,json)
                }
        }
    }
    
}
