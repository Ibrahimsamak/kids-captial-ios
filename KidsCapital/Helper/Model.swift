//
//  Model.swift
//  Chalk Store
//
//  Created by iosMaher on 7/4/18.
//  Copyright © 2018 iosMaher. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ImageST {
    
    var _id : Int
    var _url : String
    var _image : UIImage
    
    init(id: Int,url: String = "", image: UIImage = UIImage()) {
        self._id = id
        self._url = url
        self._image = image
    }
}


struct CouponST {
    
    var _id : Int
    var _title : String
    var _description : String
    var _code : String
    var _uses_no : Int
    var _start_date : String
    var _end_date : String
    var _coupon_code : String
    
    init(_ Obj : JSON) {
        self._id = Obj["id"].int ?? 0
        self._title = Obj["title"].string  ?? ""
        self._description = Obj["description"].string ?? ""
        self._code = Obj["code"].string ?? ""
        self._uses_no = Obj["uses_no"].int ?? 0
        self._start_date = Obj["start_date"].string ?? ""
        self._end_date = Obj["end_date"].string ?? ""
        self._coupon_code = Obj["coupon_code"].string ?? ""
    }
    
}

struct UserST {
    
    var _id : Int
    var _name : String
    var _email : String
    var _mobile : String
    var _username : String
    var _status_value : String
    var _user_type : Int
    
    init(_ Obj : [String:Any]) {
        self._id = Obj["id"] as? Int ?? 0
        self._name = Obj["name"] as? String ?? ""
        self._email = Obj["email"] as? String ?? ""
        self._mobile = Obj["mobile"] as? String ?? ""
        self._username = Obj["username"] as? String ?? ""
        self._status_value = Obj["status_value"] as? String ?? ""
        self._user_type = Obj["user_type"] as? Int ?? 0
    }
    
}

struct SponsoredST {
    
    var _id : Int
    var _title : String
    var _image : String
    var _email : String
    var _phone : String
    
    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._title = _Obj["title"].string ?? ""
        self._image = _Obj["image"].string ?? ""
        self._email = _Obj["email"].string ?? ""
        self._phone = _Obj["phone"].string ?? ""
    }

    init(_Obj : [String:Any]) {
        self._id = _Obj["id"] as? Int ?? 0
        self._title = _Obj["title"] as? String ?? ""
        self._image = _Obj["image"] as? String ?? ""
        self._email = _Obj["email"] as? String ?? ""
        self._phone = _Obj["phone"] as? String ?? ""
    }

    
}

struct TypeST {
    
    var _id : Int
    var _name : String
    var _parent_id : Int
    var _changeType : Int
    var _timestamp : Int
    
    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._name = _Obj["name"].string ?? ""
        self._parent_id = _Obj["parent_id"].int ?? 0
        self._changeType = _Obj["changeType"].int ?? 0
        self._timestamp = _Obj["timestamp"].int ?? 0
    }
    
    init(_Obj : [String:Any]) {
        self._id = _Obj["id"] as? Int ?? 0
        self._name = _Obj["name"] as? String ?? ""
        self._parent_id = _Obj["parent_id"] as? Int ?? 0
        self._changeType = _Obj["changeType"] as? Int ?? 0
        self._timestamp = _Obj["timestamp"] as? Int ?? 0
    }

}

struct SearchResultST {
    
    var _id : Int
    var _name : String
    var _short_description : String
    var _logo : String
    var _is_special : Bool
    var _lat : Double
    var _lng : Double
    var _open_reg : Int
    var _distance : String
    
    var _full_description : String
    var _site : String
    var _snapchat : String
    var _mobile : String
    var _whatsapp : String
    var _instagram : String
    var _share : String
    
    var _FacilitiesArray : [FacilityST] = []

    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._name = _Obj["name"].string ?? ""
        self._short_description = _Obj["short_description"].string ?? ""
        self._logo = _Obj["logo"].string ?? ""
        self._is_special = _Obj["is_special"].boolValue
        self._lat = _Obj["lat"].double ?? 0.0
        self._lng = _Obj["lng"].double ?? 0.0
        self._open_reg = _Obj["open_reg"].int ?? 0
        self._distance = _Obj["distance"].string ?? ""
        
        self._full_description = _Obj["full_description"].string ?? ""
        self._site = _Obj["site"].string ?? ""
        self._snapchat = _Obj["snapchat"].string ?? ""
        self._mobile = _Obj["mobile"].string ?? ""
        self._whatsapp = _Obj["whatsapp"].string ?? ""
        self._instagram = _Obj["instagram"].string ?? ""
        self._share = _Obj["share"].string ?? ""
        
        let _facilities = _Obj["facilities"].array ?? []
        
        var _tempArray : [FacilityST] = []
        for facility in _facilities {
            let _obj = FacilityST.init(_Obj: facility)
            _tempArray.append(_obj)
        }
        
        _FacilitiesArray = _tempArray
    }
    
    init(_Obj : [String:JSON]) {
        self._id = _Obj["id"]?.int ?? 0
        self._name = _Obj["name"]?.string ?? ""
        self._short_description = _Obj["short_description"]?.string ?? ""
        self._logo = _Obj["logo"]?.string ?? ""
        self._is_special = _Obj["is_special"]?.boolValue ?? false
        self._lat = _Obj["lat"]?.double ?? 0.0
        self._lng = _Obj["lng"]?.double ?? 0.0
        self._open_reg = _Obj["open_reg"]?.int ?? 0
        self._distance = _Obj["distance"]?.string ?? ""
        
        self._full_description = _Obj["full_description"]?.string ?? ""
        self._site = _Obj["site"]?.string ?? ""
        self._snapchat = _Obj["snapchat"]?.string ?? ""
        self._mobile = _Obj["mobile"]?.string ?? ""
        self._whatsapp = _Obj["whatsapp"]?.string ?? ""
        self._instagram = _Obj["instagram"]?.string ?? ""
        self._share = _Obj["share"]?.string ?? ""
        
        let _facilities = _Obj["facilities"]?.array ?? []
        
        var _tempArray : [FacilityST] = []
        for facility in _facilities {
            let _obj = FacilityST.init(_Obj: facility)
            _tempArray.append(_obj)
        }
        
        _FacilitiesArray = _tempArray
    }
    
}

struct FacilityST {
    
    var _name : String
    var _icon : String
    
    init(_Obj : JSON) {
        self._name = _Obj["name"].string ?? ""
        self._icon = _Obj["icon"].string ?? ""
    }
}

struct GalleryST {
    
    var _id : Int
    var _name : String
    var _main_image : String

    var _ImagesArray : [String] = []
    
    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._name = _Obj["name"].string ?? ""
        self._main_image = _Obj["main_image"].string ?? ""
        
        let _images = _Obj["images"].array ?? []
        
        var _tempArray : [String] = []
        for img in _images {
            let _img = img["name"].string ?? ""
            _tempArray.append(_img)
        }
        
        self._ImagesArray = _tempArray
    }
    
}

struct AdsCategoryST {
    
    var _id : Int
    var _name : String
    var _icon : String
    var _changeType : Int
    var _timestamp : Int

    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._name = _Obj["name"].string ?? ""
        self._icon = _Obj["icon"].string ?? ""
        self._changeType = _Obj["changeType"].int ?? 0
        self._timestamp = _Obj["timestamp"].int ?? 0
    }

    init(_Obj : [String:Any]) {
        self._id = _Obj["id"] as? Int ?? 0
        self._name = _Obj["name"] as? String ?? ""
        self._icon = _Obj["icon"] as? String ?? ""
        self._changeType = _Obj["changeType"] as? Int ?? 0
        self._timestamp = _Obj["timestamp"] as? Int ?? 0
    }
}

struct AdsListST {

    var _id : Int
    var _title : String
    var _price : Int
    var _image : String
    var _created_at : String
    var _category_id : Int
    
    var _status_value : String
    var _status_icon : String
    
    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._title = _Obj["title"].string ?? ""
        self._price = _Obj["price"].int ?? 0
        self._image = _Obj["image"].string ?? ""
        self._created_at = _Obj["created_at"].string ?? ""
        self._category_id = _Obj["category_id"].int ?? 0
        
        self._status_value = _Obj["status_value"].string ?? ""
        self._status_icon = _Obj["status_icon"].string ?? ""
    }
    
}

struct NewsST {
    
    var _id : Int
    var _title : String
    var _short_description : String
    var _thumb_image : String
    var _created_at : String
    
    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._title = _Obj["title"].string ?? ""
        self._short_description = _Obj["short_description"].string ?? ""
        self._thumb_image = _Obj["thumb_image"].string ?? ""
        self._created_at = _Obj["created_at"].string ?? ""
    }
    
}

struct AdsDetailST {
    
    var _id : Int
    var _title : String
    var _price : Int
    var _details : String
    var _status_value : String
    var _status_icon : String
    var _email : String
    var _phone : String
    var _created_at : String
    var _category_id : Int
    

    var _ImagesArray : [AdsImageST]
    
    init(_Obj : [String:JSON]) {
        self._id = _Obj["id"]?.int ?? 0
        self._title = _Obj["title"]?.string ?? ""
        self._price = _Obj["price"]?.int ?? 0
        self._details = _Obj["details"]?.string ?? ""
        self._email = _Obj["email"]?.string ?? ""
        self._phone = _Obj["phone"]?.string ?? ""
        self._created_at = _Obj["created_at"]?.string ?? ""
        self._category_id = _Obj["category_id"]?.int ?? 0

        self._status_value = _Obj["status_value"]?.string ?? ""
        self._status_icon = _Obj["status_icon"]?.string ?? ""

        let _images = _Obj["images"]?.array ?? []
        
        var _tempArray : [AdsImageST] = []
        
        for img in _images {
            let obj = AdsImageST.init(_Obj: img)
            _tempArray.append(obj)
        }
        
        self._ImagesArray = _tempArray
    }
}

struct AdsImageST {
    
    var _id : Int
    var _name : String
    
    init(_Obj : JSON) {
        self._id   = _Obj["id"].int ?? 0
        self._name = _Obj["name"].string ?? ""
    }
    
}

struct PageST {

    var _id : Int
    var _title : String
    var _changeType : Int
    var _timestamp : Int

    init(_Obj : JSON) {
        self._id = _Obj["id"].int ?? 0
        self._title = _Obj["title"].string ?? ""
        self._changeType = _Obj["changeType"].int ?? 0
        self._timestamp = _Obj["timestamp"].int ?? 0
    }
    
    init(_Obj : [String:Any]) {
        self._id = _Obj["id"] as? Int ?? 0
        self._title = _Obj["title"] as? String ?? ""
        self._changeType = _Obj["changeType"] as? Int ?? 0
        self._timestamp = _Obj["timestamp"] as? Int ?? 0
    }

}

struct ContactST {
    
    var _website : String
    var _email : String
    var _phone : String
    
    init(_Obj : [String:JSON]) {
        self._website = _Obj["website"]?.string ?? ""
        self._email = _Obj["email"]?.string ?? ""
        self._phone = _Obj["phone"]?.string ?? ""
    }
 
    init(_Obj : [String:Any]) {
        self._website = _Obj["website"] as? String ?? ""
        self._email = _Obj["email"] as? String ?? ""
        self._phone = _Obj["phone"] as? String ?? ""
    }

}

//MARK:analtics

//struct Analytics {
//    
//    static func trackEvent(withScreen screen: Screen, category: String, label: String, action: Actions, value: Int? = nil) {
//        guard
//            let tracker = GAI.sharedInstance().defaultTracker,
//            let builder = GAIDictionaryBuilder.createEvent(withCategory: category, action: action.rawValue, label: label, value: NSNumber(integerLiteral: value ?? 0))
//            else { return }
//        
//        tracker.set(kGAIScreenName, value: screen.rawValue)
//        tracker.send(builder.build() as [NSObject : AnyObject])
//    }
//    
//    static func trackPageView(withScreen screen: Screen) {
//        guard let tracker = GAI.sharedInstance().defaultTracker,
//            let builder = GAIDictionaryBuilder.createScreenView()
//            else { return }
//        
//        tracker.set(kGAIScreenName, value: screen.rawValue)
//        tracker.send(builder.build() as [NSObject : AnyObject])
//    }
//    
//    static func trackPageView(withScreen screen: Screen, _ _value:String) {
//        guard let tracker = GAI.sharedInstance().defaultTracker,
//            let builder = GAIDictionaryBuilder.createScreenView()
//            else { return }
//        
//        tracker.set(kGAIScreenName, value: screen.rawValue)
//        tracker.send(builder.build() as [NSObject : AnyObject])
//    }
//    
////    enum Actions: String {
////        case ButtonPress = “button_press”
////    }
//
//    enum Actions: String {
//        case _view = "view"
//    }
//
//    enum Screen: String {
//        
//        case SponsorPage          = "Sponsor page"
//        case HomePage             = "Home page"
//        case ProvinceSelectPage   = "Province select page"
//        case CategoriesSelectPage = "Categories select page"
//        case NurseriesListPage    = "Nurseries list page"
//        case NurseriesDetailsPage = "Nurseries details page"
//        case NewsListPage         = "News list page"
//        case NewsDetailsPage      = "News details page"
//        case AdsCategoryPage      = "Ads category page"
//        case AdsListPage          = "Ads list page"
//        case AdDetailsPage        = "Ad details page"
//        case ContactUsPage        = "Contact us page"
//        case MyAccountPage        = "My Account page"
//        
//    }
//}
