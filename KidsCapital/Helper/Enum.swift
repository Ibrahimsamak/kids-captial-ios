//
//  Enum.swift
//  Grade My Play
//
//  Created by iosMaher on 10/2/18.
//  Copyright © 2018 iosmaher. All rights reserved.
//

import Foundation

enum FilterType : String {
    case none = ""
    case Area = "AREA"
    case Category = "CATEGORY"
    case Facility = "FACILITY"
}


enum EditProfile : String {
    
    case none = ""
    case Name = "CHANGE NAME"
    case Phone = "CHANGE PHONE"
    case Username = "CHANGE USERNAME"

}
